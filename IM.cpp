// Mandel Brot Set with graphics and zoom...and it is exciting !
//0.00062969151665182674632352941176470588235294117647058823494

#include "Constants.hpp"

#include "GTexture.hpp"
#include "RGB.hpp"
#include "ThreadPool.hpp"
#include "ImageParameters.hpp"
#include "PartialCalculator.hpp"
#include "IM.hpp"

#include <iostream>
#include <iomanip>

#include <cstdlib>
#include <chrono>
#include <array>
#include <thread>
#include <functional>
#include <utility>
#include <string>
#include <algorithm>

#include <SDL2/SDL.h>
#include <gmp.h>

// template class std::array<std::array<int, Graphics::TILE_SIZE_PXLS>, Graphics::TILE_SIZE_PXLS>;
template class std::array<partial_calculator, Default::MAX_THREADS>;

std::array<std::array<int, Graphics::TILE_SIZE_PXLS>, Graphics::TILE_SIZE_PXLS>
                                              ImageManager::Itrtn_Vals = {NOT_CALCULATED};
constexpr int8_t ImageManager::NOT_CALCULATED;                            // value assigned to Itrtn_Vals when not ...

// thread items
std::array<partial_calculator, Default::MAX_THREADS> ImageManager::images;
std::array<std::future<void>, Default::MAX_THREADS> ImageManager::futures;
Current_Image ImageManager::currentImage;


// bool ImageManager::finished = false;
bool ImageManager::calculating_ItrnVals = false;
bool ImageManager::image_saved = false;


// SO THAT ALL THE PARAMETERS DON'T HAVE TO BE BOUND WHEN PUSHING TO ENQUEUE

/*template<typename R, typename C, typename... Args>
std::function<R(Args...)> objectBind(R (C::* func)(Args...), C& instance) {
    return [=](Args... args){ return (instance.*func)(args...); };
}

then:

auto callback = objectBind(&MyClass::myFunction, actualInstance);

note: you'll need overloads to handle CV-qualified member functions. ie:

template<typename R, typename C, typename... Args>
std::function<R(Args...)> objectBind(R (C::* func)(Args...) const, C const& instance) {
    return [=](Args... args){ return (instance.*func)(args...); };
}
*/
template class std::vector<std::string>;

ImageManager::ImageManager(bool automatic)
        :pool(Default::MAX_THREADS)
        ,finished{false}
        ,Automatic{automatic}
{
  mpf_set_default_prec(128);
  set_Itrn_Vals_NotCalcd();
}

bool ImageManager::ProcessImagesOnList(SDL_Renderer* renderer, std::vector<std::string>& list)
{
  static std::chrono::time_point<std::chrono::system_clock> start;

// DEBUGGING NOTES
// ImageParams is set to defaults so when initialised we get 'correct' values
// on very first call to CalculateImagesOnList

// @C Images on List:ImageManager::currentImage..MPFs-2
// 0.5
// 1.25
// -1.25
// 0
// Notice no string because nothing's been passed to ImageManager::currentImage yet

  static int count2 = 0;

  if (!list.empty() && !ImageManager::calculating_ItrnVals)
  {
    start = std::chrono::system_clock::now();
    ImageManager::currentImage.name = list.back();
    const std::string filename{(Default::PATH + ImageManager::currentImage.name + ".mlg")};
    ImageManager::currentImage.IPs.Load_IPs(filename);
    if (Automatic)
    {
      ImageManager::currentImage.IPs.UserDefined.resolution_X = 1;
      ImageManager::currentImage.IPs.UserDefined.resolution_Y = 1;
    }
    list.pop_back();
    ImageManager::calculating_ItrnVals = true;
    for(auto &it  : ImageManager::images) it.current_calc_done = false;

    ImageManager::image_saved = false;
    set_Itrn_Vals_NotCalcd();
    for(auto it = 0; it < Default::MAX_THREADS; ++it)
    {
      ImageManager::images[it].InitialiseForThisImage(it, Default::MAX_THREADS, ImageManager::currentImage.IPs);
    }
  }

  if (!ImageManager::currentImage.name.empty())
  {
    if (ImageManager::calculating_ItrnVals)
    {
      for(auto it = 0; it < Default::MAX_THREADS; ++it)
      {
        if (!ImageManager::images[it].current_calc_done)
        {
          ImageManager::futures[it] = pool.enqueue( std::bind(std::mem_fn(&partial_calculator::calcItrnVals)
                                                          ,&ImageManager::images[it]
                                                          ,std::ref(Itrtn_Vals)) );
        }
      }
      for (size_t it = 0; it < Default::MAX_THREADS; ++it)
      {
          ImageManager::futures[it].get();
          ImageManager::images[it].current_calc_done = false;
      }
// ---------------- Simple thread system at end of class ----------------
    }
  }

  size_t lastX = Graphics::TILE_SIZE_PXLS - 1;

  size_t lastY = Graphics::TILE_SIZE_PXLS - 1;
  if (ImageManager::currentImage.IPs.UserDefined.resolution_Y != 1) --lastY;
  if (ImageManager::currentImage.IPs.UserDefined.resolution_Y == 4) lastY -= 2;
  if (ImageManager::currentImage.IPs.UserDefined.resolution_Y == 10) lastY -= 8; // should be 670

// Has last value of Itrtn_Vals been calculated but the image is yet unsaved
  if ((Itrtn_Vals[lastY][lastX] != ImageManager::NOT_CALCULATED) && !image_saved )
  {
    // std::cout<<"OUTSIDE if clause count2: "<<count2<<"\n";
    ImageManager::calculating_ItrnVals = false;

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cerr<<std::endl<<"Completed in " << elapsed_seconds.count() << "s\n";

    RenderAndSaveImageNData(renderer, ImageManager::currentImage, true);
    Current_Image finished_image;
    ImageManager::currentImage = finished_image;                       // to destroy ImageManager::currentImage

// There's a reason I set this to false, I think it's so that in U_I::MainLoop the if statement works
// if (!mandelbrot_resourcer.image_manager.current_calc_done && mandelbrot_resourcer.image_manager.image_saved)
    ImageManager::finished = false;
    for(auto &it  : ImageManager::images) it.current_calc_done = false;
    for(auto &it  : ImageManager::images) it.initialised = false;

    ImageManager::image_saved = true;
  }
  return ImageManager::calculating_ItrnVals;
}


bool ImageManager::RenderAndSaveImageNData(SDL_Renderer* renderer, Current_Image & imageToProcess, bool printed)
{
  std::cerr<<"@ ImageManager::RenderAndSaveImageNData() saving "<<imageToProcess.name<<" !\n";

  bool success = true;
  RGB colour;
  colour.assignValues(imageToProcess.IPs.UserDefined.maxIterations);
  int imageCharacteristics = isImageAll_1_Colour(imageToProcess.IPs.UserDefined.resolution_X,
                                          imageToProcess.IPs.UserDefined.resolution_Y);
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  SDL_RenderClear(renderer);
  hsdl::GTexture imageTexture;
  imageTexture.PrepareToBeRendered_To(renderer, Graphics::TILE_SIZE_PXLS, Graphics::TILE_SIZE_PXLS);
  renderMandelbrot(renderer, colour, imageToProcess.IPs.UserDefined.resolution_X
                    ,imageToProcess.IPs.UserDefined.resolution_Y);
  // std::cout<<"@C Images on List in B4 save RenderAndSaveImageNData: imageToProcess."<<imageToProcess.name<<".MPFs"<<
  //               imageToProcess.IPs.MPFs<<"\n";
  // std::cout<<"1 Press Enter to Continue\n";
  // std::cin.ignore();
  // constexpr bool NEW_FILE = true;
  if (imageTexture.GetTexture() != nullptr)
  {
    if (imageToProcess.name.empty())
    {
      std::cerr<<"No image name @ ImageManager::RenderAndSaveImageNData()!\n";
      success = false;
    }
    else
    {
      const std::string filename{(Default::PATH + imageToProcess.name + ".mbt")};
      if (imageCharacteristics == Default::MANY_COLOURS)
      {
        imageTexture.SaveAsPNG(renderer, filename, Graphics::TILE_SIZE_PXLS,
                               Graphics::TILE_SIZE_PXLS);
      }
      else
      {
        std::cerr<<"\nSaving image isImageAll_1_Colour YES IT IS!!\n";
        std::string tempFileStr("NoI "+std::to_string(imageCharacteristics));
        SaveType_inFile(filename, tempFileStr, FileHandler::NEW_FILE);
      }
      imageToProcess.IPs.UserDefined.imageStateFromResolution();
      std::string logFilename = ChangeFileExtsn(filename, ".mlg");
      SaveType_inFile(logFilename, imageToProcess.IPs, FileHandler::NEW_FILE);
      imageToProcess.name.clear();
    }
  }
  else
  {
    std::cerr<<"No imageTexture at ImageManager::RenderAndSaveImageNData, nothing saved!\n";
    success = false;

  }
  ImageManager::finished = false;                               // because there'll be a new image
  return success;
}


void ImageManager::renderMandelbrot(SDL_Renderer* renderer, const RGB & colour, uint8_t xstep, uint8_t ystep)
{

  for (int itY = 0; itY < Graphics::TILE_SIZE_PXLS; itY += ystep)
  {
    for (int itX = 0; itX < Graphics::TILE_SIZE_PXLS; itX += xstep)
    {
      if(Itrtn_Vals[itY][itX] == 0) SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
      else
      {
        const int RSI = Itrtn_Vals[itY][itX];
        const int R = colour.Red[RSI];
        const int G = colour.Green[RSI];
        const int B = colour.Blue[RSI];
        SDL_SetRenderDrawColor(renderer, R, G, B, 255);
      }
      if (ystep == 1) SDL_RenderDrawPoint(renderer, itX, itY);        // coz y coords aren't cartesian
      else SDL_RenderDrawLine(renderer, itX, itY, itX, (itY + ystep - 1));
    }
  }
  return;
}

int ImageManager::isImageAll_1_Colour(uint8_t xstep, uint8_t ystep)
{
  int returnVal = Itrtn_Vals[0][0];
  // for (int itY = 0; itY < Graphics::TILE_SIZE_PXLS; itY+=ystep)
  for (size_t itY = 0; itY < Graphics::TILE_SIZE_PXLS; itY += ystep)
  {
    for (size_t itX = 0; itX < Graphics::TILE_SIZE_PXLS; itX += xstep)
    {
      if (Itrtn_Vals[itY][itX] != returnVal) return Default::MANY_COLOURS;
    }
  }
  return returnVal;
}

void ImageManager::set_Itrn_Vals_NotCalcd()
{
  // Itrtn_Vals.fill({-1});
  for(auto &row  : Itrtn_Vals)
  {
    std::fill_n(row.begin(), Graphics::TILE_SIZE_PXLS, ImageManager::NOT_CALCULATED);
  }
  return;
}

// std::vector<std::thread> threads;
// for(auto i = 0; i < Default::MAX_THREADS; ++i)
// {
//   threads.push_back( std::thread(&partial_calculator::calcItrnVals, &ImageManager::images[i]
//                                   , i, Default::MAX_THREADS, std::ref(ImageManager::currentImage.IPs)
//                                   , std::ref(Itrtn_Vals)) );
// }
// std::for_each(threads.begin(),  threads.end(),  std::mem_fn(&std::thread::join));