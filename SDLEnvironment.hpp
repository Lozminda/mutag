#ifndef SDLEVIRONMENT_HPP
#define SDLEVIRONMENT_HPP

#include "Constants.hpp"
#include "DeleterTemplates.hpp"
#include "GTexture.hpp"
#include "GWriting.hpp"
#include "Position.hpp"
#include "InfoToggleEnum.hpp"
#include "ImageParameters.hpp"
#include "StringTextures.hpp"

#include <SDL2/SDL.h>

class SDLEnvironment
{
  public:

  using Unique_SDL_Renderer = UniquePtr<SDL_Renderer, SDL_DestroyRenderer>;
  using Unique_SDL_Window = UniquePtr<SDL_Window, SDL_DestroyWindow>;

  SDLEnvironment() = delete;
  SDLEnvironment(const SDLEnvironment&) = delete;
  SDLEnvironment & operator=(const SDLEnvironment&) = delete;
  ~SDLEnvironment() = default;
  SDLEnvironment(SDLEnvironment&&) = delete;
  SDLEnvironment& operator=(SDLEnvironment&&) = delete;

  SDLEnvironment(uint16_t wPxls, uint16_t hPxls, uint16_t vp_size, Information_options  info_options);

  void Display(hsdl::GTexture & gtext,
                const Position & position, Information_options,
                ImageParams& imagePs);

  Unique_SDL_Renderer RendererPtr{nullptr};

  // static bool BLANK;

  private:

  const uint16_t bottomLeftWindowWidth = 275;

  uint16_t Mbrot_VPort_size;

  SDL_Rect mandelbrot_VP;

  StringTextures information;

  void drawFrameWork() const;

  void drawOutsideFrame() const;

  void drawFrame(uint16_t x, uint16_t y, uint16_t w, uint16_t h) const;

  void drawMouseCross(uint16_t x, uint16_t y) const;

  void drawInfoFrames() const;

  void setUp_InfoStrings();

  void setUp_InfoTextures();

  void arrangeWritingOntoTexture();

  void co_ord_InfoTextureCreate(const Position& position, ImageParams& imagesPs);


  Unique_SDL_Window windowPtr{nullptr};
};

#endif