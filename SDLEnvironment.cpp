// #include "GTexture.hpp"
#include "GWriting.hpp"
#include "InfoToggleEnum.hpp"
#include "SDLEnvironment.hpp"
#include "ImageParameters.hpp"
#include "FileHandler.hpp"
#include "StringTextures.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <string>
#include <vector>

// THIS COULD BE REWORKED AS A TEMPLATE,  TO WHICH THE USER PASSES A NUMBER OF TEXTURES TO BE DEFINED AT
// COMPILE TIME. Display() IS THEN IN THE FORMAT

// for(const auto &it  : NUMBER of textures)
// {
//   it.render(x, y);
// }
// MAYBE

SDLEnvironment::SDLEnvironment(uint16_t wPxls, uint16_t hPxls, uint16_t vp_size, Information_options info_options )
                :Mbrot_VPort_size(vp_size )
{
  windowPtr = Unique_SDL_Window{ SDL_CreateWindow( "Mandelbrot Set Universal Travelator v3", SDL_WINDOWPOS_UNDEFINED,
      SDL_WINDOWPOS_UNDEFINED, wPxls, hPxls, SDL_WINDOW_SHOWN )};

  if (!windowPtr.get())
  {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
      "SDL_CreateWindow Error: %s", SDL_GetError());
  }
  RendererPtr = Unique_SDL_Renderer{SDL_CreateRenderer(windowPtr.get(), -1,
                            SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC )};
  if( RendererPtr.get() == nullptr )
  {
    printf( "RendererPtr could not be created! SDL Error: %s\n", SDL_GetError() );
  }
  SDL_SetRenderDrawColor(RendererPtr.get(), 0, 0, 0, 0);
  SDL_RenderClear(RendererPtr.get());
  SDL_RenderPresent(RendererPtr.get());

  mandelbrot_VP.x = 282;   // 966 - 680 - 4;
  mandelbrot_VP.y = 4;    // (688-680)/2
  mandelbrot_VP.w = vp_size;
  mandelbrot_VP.h = vp_size;

  SDL_SetRenderTarget(RendererPtr.get(), nullptr);
  information.SetUp(RendererPtr.get());
  drawFrameWork();
}

void SDLEnvironment::Display(hsdl::GTexture & gtext, const Position & position,
                              Information_options info_options, ImageParams& imagePs)
{

  SDL_SetRenderTarget(RendererPtr.get(), nullptr);
  information.Render(RendererPtr.get(), position, imagePs, info_options);
  drawFrameWork();

// --------------------- Draw Mandelbrot ---------------------
// The coord and Mbrot_VPort_size are setting up the destination SDL_Rect. I agree setting the
// sourceRect outside while having the destination inside seems a bit weird
  gtext.Set_sourceRect(position.viewTopLeft_X, position.viewTopLeft_Y, Mbrot_VPort_size , Mbrot_VPort_size);

// Pretty sure this'll do nowt
  SDL_RenderSetViewport(RendererPtr.get(), &mandelbrot_VP );
  gtext.RenderSrcRct(RendererPtr.get(), 0, 0, Mbrot_VPort_size, Mbrot_VPort_size, false);    // No flipping required, should have
                                                                  // been done in SDLEnvironment::tileTextures
  drawMouseCross(position.mouseX, position.mouseY);
// NEED POSITION FOR LOADING VARIABLES FROM WITHIN LOG FILE
  SDL_RenderPresent(RendererPtr.get());
  return;
}

// ----------------------------------- PRIVATE METHODS -----------------------------------

void SDLEnvironment::drawFrameWork() const
{
    SDL_RenderSetViewport(RendererPtr.get(), nullptr);
    drawInfoFrames();
    drawOutsideFrame();
    return;
}

void SDLEnvironment::drawOutsideFrame() const
{
  drawFrame(0, 0, 965, 687);
  return;
}

void SDLEnvironment::drawMouseCross(uint16_t x, uint16_t y) const
{
  SDL_SetRenderDrawColor(RendererPtr.get(), 85, 85, 85, 120);
  SDL_RenderDrawLine(RendererPtr.get(), x, 0, x, Mbrot_VPort_size);
  SDL_RenderDrawLine(RendererPtr.get(), 0, y, Mbrot_VPort_size, y);
  return;
}

void SDLEnvironment::drawFrame(uint16_t x, uint16_t y, uint16_t w, uint16_t h) const
{
  SDL_SetRenderDrawColor(RendererPtr.get(), 255, 255, 255, 255);
  SDL_RenderDrawLine(RendererPtr.get(), x, y, x + w, y);
  SDL_RenderDrawLine(RendererPtr.get(), x + w, y, x + w, y + h);
  SDL_RenderDrawLine(RendererPtr.get(), x, y + h, x + w, y + h);
  SDL_RenderDrawLine(RendererPtr.get(), x, y, x, y + h);
  SDL_SetRenderDrawColor(RendererPtr.get(), 150, 150, 150, 255);
  SDL_RenderDrawLine(RendererPtr.get(), x + 2, y + 2,
                                                x + w - 2, y + 2);

  SDL_RenderDrawLine(RendererPtr.get(), x + w - 2, y + 2,
                                                x + w - 2, y + h - 2);
  SDL_RenderDrawLine(RendererPtr.get(), x + 2, y + h - 2,
                                                x + w - 2, y + h - 2);

  SDL_RenderDrawLine(RendererPtr.get(), x + 2, y + 2,
                                                x + 2, y + h - 2);
  return;
}

void SDLEnvironment::drawInfoFrames() const
{
  drawFrame(Default::frameThickness, Default::frameThickness, bottomLeftWindowWidth, bottomLeftWindowWidth);
  drawFrame(Default::frameThickness, 282, bottomLeftWindowWidth, 399);
  return;
}