<h1>Mandelbrot Universe Travelator and Generator</h1>

<h2>GENERALLY:</h2>

Sort out some of the APIs: Some are a bit clunky to say the least
Parts of this program are modified imperative style and also API
has changed as functionality has changed. In some areas there was some bad planning
eg Movement in User_Interface

Sort Comments

Sort out nomenclature

Sort out file system, ie some classes should go in an Ancillaries folder

The entire repo needs some serious tidying

Learn CMake !

The makefile for this project is also fairly hideous, it's a generic makefile 
for all projects hence it's generality.

Send parts of code to code review website.

***

UnThread MUTAG for debugging or single core machines & sort threadpool ✓

✓ Debug calling 'RenderAndSaveImageData()' occurs twice on level 1 when there's no imagePs ✓

Should only be called once. Also logfile MPF::Topand Bottom are very large but the difference
is 1.5 or 2.5✓

***

MUTAG super class which would have method MainLoop()


Constants.hpp separate into Defaults:: & Graphics::✓
Maybe some should go back into 'their' classes (if only used in one class)

***

ImageParams & Display & redo infoTextures. Diplay should prob be just getting IPs from 
small_textures[index] and then just using those values.

Current imagePs vs Loaded imagePs also userdefined IPs should be separate from IP.MPFs
The IP.MPF's pertain to each image where as the UserDefined IPs (ImageParams) are just that:
each image has some User defined properties but these can change,  ie number of iterations, 
what percentage of the image is gonna be reproduced etc.

Combine MPFs with small_texture.imagePs ✓

Deal with missing .mlg files.

***

On reflection,  not sure that this s such a great idea:
Textures going from 3 by 3 grid all the time to:
```
  3
* 2 *       * * *
* 1 *  =>   * 3 *
* * *       * 2 *
```
as opposed to how it is now
```
1 2 3      4 5 6
4 5 6  =>  7 8 9
7 8 9      A B C
```

***

Sort command line and automatic MUTAG looking for 'U' in logfiles

Refactor SDLEnvironment

Image being flipped ??? ✓ DONE

The black line at y = 0
