<h2>Mandelbrot Universe Travelator and Generator</h2>

<h3>1st Disclaimer</h3>

It might be worth reading the TODO file first or certainly in conjunction
with this README file. Stuff hasn't been done due to time/limits of 
my knowledge. 

<h3>PROGRAM STRUCTURE</h3>

MUTAG.cpp is the final executable from which a *UserInterface* object is created.
It's *MainLoop()* 'is where the action happens' 
*UserInterface* processes input via keyboard and mouse changing *position* (*Ancillaries/Position*)in the filesystem (*Ancillaries/LetterCords*), Position on the large Mandelbrot Texture 
(*Graphics/SmallTexture & Mandelbrot_Resourcer*) and finally the mouse position.

UserInterface::MainLoop() also stores a list of images to be calculated and runs a multithreaded
process (*ProcessImagesOnList()* & *Ancillaries/PartialCalculator*) to do those calculations as well as rendering the current image at the location in the
Mandelbrot set, specified by position,  to the screen (*SDLEnvironment graphicsWindow*)

*Mandelbrot_Resourcer* creates a *CompleteTexture* which is larger than the main viewport, to
enable rapid verticle and horizontal travel through the set. OrganiseTileData() works out
which .PNG files need to be loaded (or calculated) depending on the users postion in the file structure and within the Mandelbrot set. Texturise renders those smaller tiles to *CompleteTexture*
The *ImageManager* is responsible for calculating and saving missing files from the missing files list: *images2B_calculated*

*SDLEnvironment graphicsWindow* renders the Mandelbrot texture, the current information texture and the map texture in the three corresponding viewports as well as the various framing as well as the system window. This is in the process of being re-written to sort out the API. 

<h3>LIBRARIES, COMPILER and HARDWARE:</h3>

Firstly I only have 32bit Machines (i386)!

I've used SDL2 (thanks to Lazy Foo tutorial). I could have used SFML (?) but
SDL2 came onto my radar first.

When zooming into the set increased number precision (as well as iterations per pixel)
is required and for this I've used GMP aka gmplib.
I couldn't get the C++ version to compile on my machine at the time of writing
the class that did the calculations (I succeeded later on a different machine)
hence am using the C API and this has effected a few design decisions. 
The C++ version of gmplib is much easier to use a far more flexible,  but this was not 
available to me at the time of writing this code.

I've generally used clang to compile. Trying to persuade clang6 to compile on my 
operating system (and thus give me access to C++17) is nigh on impossible
though I do have gcc 7.5.0. Clang does give much nicer error messages.
Compiled with clang and then linked with g++-7.

<h3>NOTES re NOMENCLATURE:</h3>
I've used names starting with a small letter for private methods.
Methods starting with a capital are public.

Variables starting with a capital are used across classes or classes
I've written.

Many standards suggest that small case and under scores should be used,
but I've ignored that advice so that anything that is little case and underscore
is either STL or STD (or a local variable) where as the Camel/Pascal case stuff
is more likely to be mine (SDL2 is all Pascal case).
I've used the odd underscore in the Camel/Pascal case to aid readability here and there.
Anything that's mixture of Camel with underscores you know is mine !

I haven't used 'm_' for member. (It is used in ThreadPool.hpp which is largely
**not** my work. I've credited it's author in the comments in ThreadPool.hpp)

Snake case for constants.


<h3>GENERAL WAFFLE:</h3>
MUTAG is at it's core a file structure generator and navigator,  these files
being images of the Mandelbrot set. As the Mandelbrot set is symetrical about
the x axis, images above the x axis can be flipped when the user is viewing
parts of the set below the x axis therefore reducing the total number of
images by half.

As one zooms into the set the number of images per zoom level quadruples
(but only half of those are required,  see comment above re x axis).
The first level, level 1 has one image, level 2,  2, level 3 8, level 4 32, 
level 5,  128. etc.

For it to work, the directory structure must be maintained as well as the file
nomenclature.

Some reason for some of the design decisions:

So why store the Mandelbrot Set as images?
    Much quicker to load an image than calculate it each time, especially in the 
    deeper realms. Once the image is calculated once, no further work needs doing

Why not calculate all the images for the entire set:
    The amount of images per 'zoom level' get big quickly. Even after zooming
    only 10 times, to calculate and render each image would take several hours
    and it's 100s of thousands of files.

To overcome the amount of time to generate images and space required that these
images would take up, instead of generating them all for the entire M'brot set
(theorectically, well actually infinite) my thought was let the user dictate
which images are to be generated, using the mouse or keyboard, the user can move right
left, up and down in the level or zoom in or out, moving a level up or down. 

The first 7 levels or so could be generated quite easily. As the user delves into
the set, the user can adjust the number of iterations per image as they go as well
as the amount of each image that's produced. Once each pixel is being iterated
100,000's of times and each image is taking several hours to produce it might be
handy to only produce 10% of that image enough to see where you are.
Once you've completed your journey of partially created sets/images the program can be
set to automatic. The program works it's way through the uncompleted images
using the .mlg files (logfile for each image) to generate a complete image,  plus the
surrounding eight images. You'd then be able to re-explore at your leisure seeing
the complete images,  without having to wait for them to be drawn,  zooming in and
out in real time.
A map in the top left shows the user where they've been, and where hasn't been explored.  
(I've yet to implement this)

Each image is named according to it's place within the overall set of images.
The first number is the level, the first 6 letters (this number may have to be larger)
the file position y-cordinate and the last six letters the fileposition x-coord.
Thus the one image of level one (the classic "M'brot image") is 000001aaaaaaaaaaaa.mbt
The level 2 images are 000002aaaaaaaaaaaaaa.mbt 000002aaaaaaaaaaaaab.mbt

<h4><b>General Waffle is Unfinished</b></h4>