
#include "FileHandler.hpp"

#include <vector>
#include <string>
#include <iostream>
#include <memory>
#include <typeinfo>
#include <dirent.h>
#include <sstream>
#include <fstream>

#include <cstddef>

#include <gmp.h>
// #include <gmpxx.h>


std::string FileHandler::directory_name;

FileHandler::FileHandler(const std::string& name)
{
  FileHandler::directory_name = name;
}

std::string FileHandler::ChangeFileExtsn(const std::string& filename, const std::string& newEtn)
{
  if (filename.empty())
  {
    std::cerr<<"Empty filename to FileHandler::ChangeFileExtsn()\n";
    return filename;
  }
  std::string returnStr{filename};
  std::size_t found = returnStr.rfind(".");
  if (found != std::string::npos)
  {
    returnStr.erase(found, std::string::npos);
    returnStr += newEtn;
  }
  // std::cerr<<"FileHandler::CHANGEFILEEXTSN returnStr:"<<returnStr<<"\n";

  return returnStr;
}

std::string FileHandler::RemovePath(const std::string& filename, const std::string& path)
{
  if (filename.empty())
  {
    std::cerr<<"Empty filename to FileHandler::RemovePath()\n";
    return filename;
  }
  if (path.empty())
  {
    std::cerr<<"Empty path to FileHandler::RemovePath()\n";
    return filename;
  }
  std::string returnStr{filename};
  std::size_t found = returnStr.find(path);
  if (found == 0)
  {
    returnStr.erase(0, path.length());
  }
  else
  {
    std::cerr<<path<<" not at beginning of filename at FileHandler::RemovePath!\n";
    return filename;
  }
  // std::cerr<<"FileHandler::RemovePath returnStr:"<<returnStr<<"\n";
  return returnStr;
}


bool FileHandler::OpenDirectory(const std::string& dir_name)
{
  bool opened = false;
  // std::shared_ptr<DIR> directory(nullptr);
  if (!dir_name.empty())
  {
    directory = std::shared_ptr<DIR>{opendir(dir_name.c_str()), [](DIR *ptr){closedir(ptr);}};
    if (directory == nullptr) std::cout<<dir_name<<" failed to open in FileHandler::openDirectory\n";
    else opened = true;
  }
  else std::cout<<"Empty argument passed to FileHandler::openDirectory\n";
  return opened;
}

void FileHandler::DirContents(const std::string& dir_name)
{
  struct dirent *dirElement;
  if (OpenDirectory(dir_name))
  {
    while ((dirElement = readdir(directory.get())) != nullptr)
    {
      directoryElements.push_back(*dirElement);
    }
  }
  else std::cout<<dir_name<<" not valid at FileHandler::DirContents\n";
  return;
}

// Need to add a pattern match too !

std::vector<std::string> FileHandler::CreateList(bool printOut, FileHandler::EntityType etype)
{
  std::vector<std::string> returnList;
  if (directoryElements.size() != 0)
  {
    if (printOut) std::cout<<"Elements added to list are:\n";
    for(const auto &element  : directoryElements)
    {
      std::string temp{element.d_name};
      if (match(element, etype))
      {
        returnList.push_back(temp);
        if (printOut) std::cout<<temp<<"\n";
      }
    }
  }
  else std::cout<<"Nothing to make a list out of @ FileHandler::CreateList()!\n";
  return returnList;
}

std::vector<std::string> FileHandler::StringsFromFile(const std::string& filename)
{
  std::vector<std::string> total_fileContents;
  std::ifstream fileStream;
  fileStream.open(filename.c_str());
  if ( ! fileStream.is_open() ) std::cout<<"Can't open file: "<<filename<<
    " at FH::stringsFromFile.\n";
  else
  {
    // std::cout<<"Opening File "<<filename<<"\n";
    while ( ! fileStream.eof() )
    {
      std::string lineContents;
      getline(fileStream, lineContents);

      if(!lineContents.empty())
        total_fileContents.push_back(lineContents);
    }
    fileStream.close();
  }
  if (total_fileContents.empty()) std::cout<<"Empty file\n";
  return total_fileContents;
}

// ----------------------------------- PRIVATE METHODS -----------------------------------

bool FileHandler::match(const dirent& element, EntityType etype)
{
  if (etype == All) return true;
  if ((element.d_type == DT_BLK) && (etype == BlockDevice)) return true;
  if ((element.d_type == DT_CHR) && (etype == CharDevice)) return true;
  if ((element.d_type == DT_DIR) && (etype == Folder)) return true;
  if ((element.d_type == DT_FIFO) && (etype == NamedPipe)) return true;
  if ((element.d_type == DT_LNK) && (etype == SymbolicLink)) return true;
  if ((element.d_type == DT_REG) && (etype == File)) return true;
  if ((element.d_type == DT_SOCK) && (etype == Socket)) return true;
  if ((element.d_type == DT_UNKNOWN) && (etype == Unknown)) return true;
  return false;
}