#ifndef FILEHANDLER_TPP
#define FILEHANDLER_TPP

#ifndef FILEHANDLER_HPP
#error __FILE__ should only be included from FILEHANDLER.hpp.
#endif // FILEHANDLER_HPP

#define _GNU_SOURCE

#include <utility>
#include <iostream>
#include <dirent.h>
#include <fstream>


// enum EntityType{BlockDevice, CharDevice, Folder, NamedPipe, SymbolicLink, File, Socket, Unknown, LAST};


template<typename TYPE>
TYPE FileHandler::NumberOf(bool printOut, EntityType etype,
                            const std::string& pattern, const std::string& dir_name)
{
  TYPE elementsThatMatch = 0;
  if (directoryElements.size() == 0) DirContents(dir_name);
  if (printOut) std::cout<<"List of entites that match "<<pattern<<" are:\n";
  for(const auto element  : directoryElements)
  {
    if (match(element, etype))
    {
      const std::string name{element.d_name};
      if (pattern.empty())
      {
        ++elementsThatMatch;
        if (printOut) std::cout<<name<<"\n";
      }
      else if (std::string(element.d_name).find(pattern) != std::string::npos)
      {
        ++elementsThatMatch;
        if (printOut) std::cout<<name<<"\n";
      }
    }
  }
  return elementsThatMatch;
}

//Type T must have operator<< overloaded for this to work !

template <typename T>
bool FileHandler::SaveType_inFile(const std::string& filename, T& param,
                                  const bool newFile)
{
  bool sucess = true;
  // std::cout<<"Opening file: "<<filename<<"\n";
// There need to be some kind of options.ATM file will be overwritten
  if (newFile) filestream.open(filename.c_str(), std::ofstream::out
                                |std::ofstream::trunc);
  else filestream.open(filename.c_str(), std::ofstream::out
                        |std::ofstream::app | std::ios_base::app);
  if (filestream.is_open())
  {
    filestream<<param<<std::endl;
    filestream.close();
  }
  else
  {
    std::cerr<<"Error opening file:"<<filename<<" @FileHandler::SaveType_inFile!\n";
    sucess = false;
  }
  return sucess;
}

#endif