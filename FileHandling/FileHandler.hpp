#ifndef FILEHANDLER_HPP
#define FILEHANDLER_HPP

#include <vector>
#include <string>
#include <memory>
#include <map>
#include <fstream>
#include <dirent.h>

#if __linux__
  #include <unistd.h>                                             // for access()
#endif

class FileHandler
{
  public:

  using stringVec = std::vector<std::string>;
  using vecOf_stringVec = std::vector<stringVec>;

  enum EntityType{All, BlockDevice, CharDevice, Folder, NamedPipe, SymbolicLink, File, Socket, Unknown, LAST};

  FileHandler() = default;

  FileHandler(const std::string& name);

  constexpr static bool NEW_FILE = true;

#ifdef __linux__
  inline bool DoesFileExist(const std::string& name)
  {
    return static_cast<bool>(access(name.c_str(), F_OK) != -1);
  }
#endif

  std::string ChangeFileExtsn(const std::string& filename, const std::string& newEtn);
  std::string RemovePath(const std::string& filename, const std::string& path);

  template<typename TYPE>
  TYPE NumberOf(bool printOut, EntityType etype, const std::string& pattern = "",
                                const std::string& dirName = FileHandler::directory_name);

  template <typename T>
  bool SaveType_inFile(const std::string& filename, T& param, const bool newFile = false);

  std::vector<std::string> StringsFromFile(const std::string& filename);


// DirContents() updates directoryElements of FileHandler::directory_name unless
// a parameter is supplied an then its the contents of the supplied folder

  void DirContents(const std::string& dirName = FileHandler::directory_name);


// OpenDirectory() opens a directory named the contents of FileHandler::directory_name
// unless a parameter is supplied an then its that directory

  bool OpenDirectory(const std::string& dirName = FileHandler::directory_name);

  std::vector<std::string> CreateList(bool printOut, FileHandler::EntityType etype = All);

  static std::string directory_name;

  private:
  std::shared_ptr<DIR> directory{nullptr};
  std::vector<dirent> directoryElements;
  std::ofstream filestream;

  bool match(const dirent & element, EntityType etype);
};

#include "FileHandler.tpp"

#endif

  // template <typename T, typename... Types>
  // bool send_toFile(T first_param, Types... params);

  // bool send_toFile();

  // template <typename T, typename... Types>
  // bool SaveType_inFile(const std::string& filename, T first_param,
  //                       Types... params);