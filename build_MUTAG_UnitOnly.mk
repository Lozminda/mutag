# TODO INCLUDE PTHREAD VARIABLE
# Test this out on OMBblarblar13, the one ou got thread to work on

# This variable might be 'overwritten' by a command line arg
CC = clang++-3.8
GMP = false
THREAD = false
SDL2FLAGS = -lSDL2 -lSDL2_image -lSDL2_ttf 
GMPFLAGS =  -lgmp -lgmpxx
THREADFLAGS = -pthread
LDTHEADFLAGS = -lpthread
SANITIZEFLAGS =  -fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls

# LDFLAGs for -L rather than -l LDFLAGS = ...
LDLIBS = -lc++ -lm -lgcc_s -lgcc -lc

# Sorting the different flags out for different compilers and modes
ifeq ($(CC),g++-7)
  CXXFLAGS = -std=c++17 -w -O0 -g -DDEBUG
  sdl2:LDLIBS += $(SDL2FLAGS)
else
  CXXFLAGS = -std=c++14 -stdlib=libc++ -O0 -ggdb3 -DDEBUG -w
#   CXXFLAGS = -std=c++14 -stdlib=libc++ -w -O0 -ggdb -DDEBUG
  sdl2:LDLIBS += $(SDL2FLAGS)
endif

ifeq ($(THREAD),true)
  CXXFLAGS += $(THREADFLAGS)
  LDLIBS += $(LDTHEADFLAGS)
endif

ifeq ($(GMP),true)
  LDLIBS += $(GMPFLAGS)
endif

SRCS := $(addsuffix .cpp,$(TARGET))
OBJS := $(addsuffix .o,$(TARGET))

sdl2: $(TARGET)
default:$(TARGET)

%.o: %.cpp
	$(CC) $(CPPFLAGS) $(CXXFLAGS) -c $< ; pwd

$(TARGET): $(OBJS)
	g++-7 $^ $(LDLIBS) -o $@ 
