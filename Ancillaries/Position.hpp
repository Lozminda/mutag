#ifndef POSITION_HPP
#define POSITION_HPP
// #define NDEBUG

#include "LetterCoords.hpp"

#include <string>

struct Position
{
    public:

    static const ltcd::LetterCoords FILE_SYS_MIN;

    Position();
    Position(int vtl_x, int vtl_y, int mx, int my, uint64_t flev,
              const std::string& fpx, const std::string& fpy,
              bool sTBlowXax, int chng);


    ~Position() = default;

    Position(Position const& other) = default;
    Position& operator=(Position const& rhs) = default;

    void Initialise(int vtl_x, int vtl_y, int mx, int my, uint64_t flev,
              const std::string& fpx, const std::string& fpy,
              bool sTBlowXax, int chng);

    void Initialise(const std::string&  vtl_x, const std::string&  vtl_y, const std::string& mx,
              const std::string&  my, const std::string&  flev, const std::string& fpx,
              const std::string& fpy, const std::string&  sTBlowXax, const std::string& chng);

    std::string fileXY_level_str() const;


    friend std::ostream & operator << (std::ostream&, const Position&);


// viewTopLeft, position top left corner of window in relation to large texture. Eg On a 2040x2040
// large texture, viewTopLeft_X=680, wPY=680 will dsplay the middle section of that texture (680-1360)
// current screensize being a global consant of value 680. posOnMandel_textureX&Y = viewTopLeft_X&Y + mouseX&Y
// Have this as Postion

// Moves the 'view' to the left across largeTexture and handles boundries ie is the mouse or
// cross hairs at the ridehandside of the screen, is the view at the right handside of
// largeTexture and updates viewTopLeft_X&Y, fileSystem_x&y, and largeTexture

// INT ISN'T THE BEST TYPE FOR THESE VALUES SHOULD BE UINT16_T OR SOMETHING NONE ARE
// LARGE EXCEPT FOR fileSystem_x & y
    // bool createMandelbrot;

    int viewTopLeft_X;
    int viewTopLeft_Y;

    int mouseX;
    int mouseY;

    bool small_TextureBelowXaxis;

// Positions within the file system eg 'aaaaaa', 'aaaaaa' or 'aadfxy', 'zdefti'
// using fileSystem_x and y as the centre of the texture or work out if at the edge or in a corner
// Think of a noughts and crosses board (tic tak toe) fileSystem_x/y is the centre with the other
// file tiled around eg level 3 if above the x axis, fileSystem_x = 'aaaaab' and fileSystem_y = 'aaaaab'
// the surrounding files/smallTextures would be (fP_x/y in curly brackets {}, [] means texture
// flipped) as follows:

//            003aaaaaaaaaaaa   003aaaaabaaaaaa   003aaaaacaaaaaa
//            003aaaaaaaaaaab  {003aaaaabaaaaab}  003aaaaacaaaaab
//           [003aaaaaaaaaaab] [003aaaaabaaaaab] [003aaaaacaaaaab]

// If fileSystem_x and y were in the bottom left corner the following filenames would be returned:

//              [003aaaaacaaaaab]  [003aaaaadaaaaab]
//              [003aaaaacaaaaaa] {[003aaaaadaaaaaa]}

    ltcd::LetterCoords fileSystem_x;
    ltcd::LetterCoords fileSystem_y;

// What zoom level user is currently at with the full first mandelbrot image at 00000001,
// aaaaaa, aaaaaa being fileLevel = 1
    uint64_t fileLevel;


// Moves the 'view' to the left across largeTexture and handles boundries ie is the mouse or
// cross hairs at the ridehandside of the screen, is the view at the right handside of
// largeTexture and updates viewTopLeft_X&Y, fileSystem_x&y, and largeTexture

    int change;
};


#endif