#ifndef ImageParamHelperFuncs_HPP
#define ImageParamHelperFuncs_HPP

#include <string>

#include <gmp.h>
// #include <gmpxx.h>

namespace iphf
{

std::string mpfToStr( const mpf_t param_mpf);
std::string mpfToStr( mpf_t param_mpf);
void stringToMPF(mpf_t param_mpf, std::string& input);

}

#endif