#ifndef IMAGEPARAMETERS_HPP
#define IMAGEPARAMETERS_HPP

#include "Constants.hpp"
#include "ImageParamHelperFuncs.hpp"

#include <cstdint>
#include <array>
#include <string>

#include <gmp.h>

struct Position;

struct MPF_s
{
  MPF_s();
  MPF_s(mpf_t, mpf_t, mpf_t, mpf_t, mpf_t);
  MPF_s(signed l, signed r, signed t, signed b, signed i);
  MPF_s(double l, double r, double t, double b, double i);

  ~MPF_s() = default;

  MPF_s(const MPF_s&) = default;
  MPF_s & operator=(const MPF_s&) = default;
  MPF_s(MPF_s&&) = default;
  MPF_s& operator=(MPF_s&&) = default;

  void Initialise(double l, double r, double t, double b, double i);

  mpf_t LHSide;
  mpf_t RHSide;
  mpf_t Bottom;
  mpf_t Top;
  mpf_t ImageWidth;

  void calc_ImageWidth(double, uint64_t level);
  friend std::ostream & operator << (std::ostream&, MPF_s&);
  friend std::ostream & operator << (std::ostream&, const MPF_s&);
};


struct UserDefined_s
{
  UserDefined_s();
  UserDefined_s(uint8_t resX, uint8_t resY, char state, uint32_t prec);
  UserDefined_s(const UserDefined_s&) = default;

  void Initialise(const UserDefined_s& user_params);

// Do I really wanna compare image state in operator == ??
  friend bool operator==(const UserDefined_s& lhs, const UserDefined_s& rhs) noexcept
  {
    return (lhs.resolution_X == rhs.resolution_X) && (lhs.resolution_Y == rhs.resolution_Y)
            && (lhs.imageState == rhs.imageState) && (lhs.maxIterations == rhs.maxIterations);
  }

  friend bool operator!=(const UserDefined_s& lhs, const UserDefined_s& rhs) noexcept
  {
    return !operator==(lhs,rhs);
  }

  void toggle();
  void imageStateFromResolution();
  uint8_t resolution_X;
  uint8_t resolution_Y;
  char imageState;
  uint32_t maxIterations;                                           //Should be uint64_t?
  friend std::ostream & operator << (std::ostream&, const UserDefined_s&);

  constexpr static char COMPLETE = 'C';
  constexpr static char NOT_CALCULATED = 'N';
  constexpr static char PARTIALLY_CALCULATED = 'P';

};

struct ImageParams
{
  UserDefined_s UserDefined;
  MPF_s MPFs;
  ImageParams();
  ImageParams(mpf_t, mpf_t, mpf_t, mpf_t, mpf_t);
  ImageParams(signed l, signed r, signed t, signed b, signed i);
  ImageParams(double l, double r, double t, double b, double i);
  ImageParams(uint8_t resX, uint8_t resY, char state, uint32_t prec);
  ImageParams(mpf_t, mpf_t, mpf_t, mpf_t, mpf_t, uint8_t resX,
              uint8_t resY, char state, uint32_t prec);
  ImageParams(mpf_t l, mpf_t r, mpf_t t, mpf_t b, mpf_t i,
              const UserDefined_s& user_params);
  ImageParams(double l, double r, double t, double b, double i,
              const UserDefined_s& user_params);
  ImageParams(double l, double r, double t, double b, double i, uint8_t resX,
              uint8_t resY, char state, uint32_t prec);

  void Initialise(double l, double r, double t, double b, double i,
                          const UserDefined_s& user_params);

  void Initialise(const std::string& t, const std::string& b, const std::string& l, const std::string& r,
                  const std::string& i, const std::string& resX, const std::string& resY,
                  const std::string& state, const std::string& prec);

  ImageParams Load_IPs(const std::string& filename);
  ImageParams Load_IPs(const Position& position);


  static bool ImageParamChanged;
  static bool maxIterationsHaveChanged;

  friend std::ostream & operator << (std::ostream&, ImageParams&);
  // friend ImageParams Load_IPs(const std::string& filename);
  // friend ImageParams Load_IPs(const Position& position);

};

#endif

