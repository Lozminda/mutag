// These two functions sholdn't really be a part of LetterCoords

// SAVE ALL ONE COLOUR FILES WITH NUMBER OF ITERATIONS TOO AS NUMBER OF 
// ITERATIONS HAS AN EFFECT ON THE COLOUR SCHEME,  WHICH IS THE POINT OF RGB
// THEY CAN BE REDIFINED RE NUM OF ITERATIONS see TileOraginser.cpp

#ifndef LETTERCOORDS_TPP
#define LETTERCOORDS_TPP

#ifndef LETTERCOORDS_HPP
#error __FILE__ should only be included from LetterCoords.hpp.
#endif // LetterCoords_HPP

#include <limits>

namespace ltcd
{

// For both boundries letters needs to be set to 'aaaaaa'
template <typename T>
LetterCoords& LetterCoords::PowerTwo(T level)
{
  if ( (level < 0) || (level > std::numeric_limits<int>::max()) )
      // throw std::runtime_error ("")    Also should use this,  should use if statement
    throw std::out_of_range ("@ LetterCoords::PowerTwo: INT_MAX < parameter < 0\n");
  else
  {
    letters.fill('a');
  // should I be doing in this way ? Could be more efficient ? TO DO 
    for (size_t it = 1; it < pow(2,level); ++it, ++(*this));
  }
  return *this;
}

// template <typename T>
// LetterCoords& LetterCoords::Y_boundry(T level)                   // level is half again in y direction
// {
//   assert(level >= 1);
//   letters.fill('a');
//   if (level < 2) level = 2;
//   for (size_t it = 1; it < pow(2,(level-2)); ++it, ++(*this));
//   return *this;
// }

#ifdef DEBUG                                                   // -DDEBUG flag for compiler
template <typename RETURN_T>
RETURN_T LetterCoords::nlmax()
{
  return std::numeric_limits<maybeUInt64_ish>::max();
}
#endif

} // ENDOF namespace

#endif