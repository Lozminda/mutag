#include "Constants.hpp"
#include "Position.hpp"
#include "LetterCoords.hpp"

// posLayerX, posLayerY, mx, my, lt, xlc, ylc, flipped

const ltcd::LetterCoords Position::FILE_SYS_MIN;

Position::Position()
        :viewTopLeft_X{Default::POS_ON_LARGE_TEXTURE_X}
        ,viewTopLeft_Y{Default::POS_ON_LARGE_TEXTURE_Y}
        ,mouseX{Default::MOUSE_POS_X}
        ,mouseY{Default::MOUSE_POS_Y}
        ,fileLevel{Default::START_LEVEL}
        ,small_TextureBelowXaxis{Default::FLIPPED}
        ,change{Default::CROSSHAIR_SPEED}
{}

Position::Position(int vtl_x, int vtl_y, int mx, int my, uint64_t flev,
                    const std::string& fpx, const std::string& fpy,
                    bool sTBlowXax, int chng)
                  :viewTopLeft_X{vtl_x}
                  ,viewTopLeft_Y{vtl_y}
                  ,mouseX{mx}
                  ,mouseY{my}
                  ,fileLevel{flev}
                  ,small_TextureBelowXaxis{sTBlowXax}
                  // ,createMandelbrot{false}
                  ,change{chng}
{
  fileSystem_x.from_string(fpx);
  fileSystem_y.from_string(fpy);
}

void Position::Initialise(int vtl_x, int vtl_y, int mx, int my, uint64_t flev,
                    const std::string& fpx, const std::string& fpy,
                    bool sTBlowXax, int chng)
{
  viewTopLeft_X = vtl_x;
  viewTopLeft_Y = vtl_y;
  mouseX = mx;
  mouseY = my;
  fileSystem_x.from_string(fpx);
  fileSystem_y.from_string(fpy);
  fileLevel = flev;
  small_TextureBelowXaxis = sTBlowXax;
  // createMandelbrot = false;
  change = chng;
}

void Position::Initialise(const std::string&  vtl_x, const std::string&  vtl_y, const std::string& mx,
                  const std::string&  my, const std::string&  flev, const std::string& fpx,
                  const std::string& fpy, const std::string&  sTBlowXax, const std::string& chng)
{
  // new_variable.push_back(static_cast<uint32_t>(std::stoul(str)));
  viewTopLeft_X = static_cast<int>(std::stoi(vtl_x));
  viewTopLeft_Y = static_cast<int>(std::stoi(vtl_y));
  mouseX = static_cast<int>(std::stoi(mx));
  mouseY = static_cast<int>(std::stoi(my));
  fileSystem_x.from_string(fpx);
  fileSystem_y.from_string(fpy);
  fileLevel = static_cast<uint64_t>(std::stoll(flev));
  small_TextureBelowXaxis = static_cast<int>(std::stoi(sTBlowXax));
  // createMandelbrot = false;
  change = static_cast<int>(std::stoi(chng));
}

std::string Position::fileXY_level_str() const
{
  return (fileSystem_y.toString() + fileSystem_y.toString() + std::to_string(fileLevel));
}
//posLayerX, posLayerY, mx, my, lt, xlc, ylc, flipped

std::ostream & operator << (std::ostream &os, const Position & param)
{
   os<<param.viewTopLeft_X<<std::endl;
   os<<param.viewTopLeft_Y<<std::endl;
   os<<param.mouseX<<std::endl;
   os<<param.mouseY<<std::endl;
   os<<param.fileLevel<<std::endl;
   os<<param.fileSystem_x.toString()<<std::endl;
   os<<param.fileSystem_y.toString()<<std::endl;
   os<<param.small_TextureBelowXaxis<<std::endl;
   os<<param.change<<std::endl;
  return os;
}
