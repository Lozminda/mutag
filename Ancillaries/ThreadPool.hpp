//If this works, full credit to https://github.com/PaulRitaldato1/ThreadPool
#ifndef THREADPOOL_HPP
#define THREADPOOL_HPP

#include <thread>
#include <vector>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <future>
#include <atomic>
#include <type_traits>
#include <typeinfo>

class ThreadPool
{
  public:

  ThreadPool()
  {
    m_shutdown.store(false, std::memory_order_relaxed);
    //m_shutdown = false;
    createThreads(1);
  }

  ThreadPool(std::size_t numThreads)
  {
    m_shutdown.store(false, std::memory_order_relaxed);
    //m_shutdown = false;
    createThreads(numThreads);
  }

  ~ThreadPool()
  {
    m_shutdown.store(true, std::memory_order_relaxed);
    //m_shutdown = true;
    m_notifier.notify_all();

    for (std::thread& th : m_threads)
    {
      th.join();
    }
  }

  template <typename Func, typename... Args>
  auto enqueue(Func&& f, Args&&... args)
  {


// **************************************************************************
    #if __cplusplus == 201402L
        using RetType = std::result_of_t<Func(Args...)>;
    #elif __cplusplus == 201703L
        using RetType = std::invoke_result_t<Func, Args...>;        // C++17
    #endif

// **************************************************************************

    // typedef decltype(f(args...)) retType;

// This line didn't work,  so the line below is my modification
// auto task = std::make_shared<std::packaged_task<RetType()>>([&f, &args...]() { return f(std::forward<Args>(args)...); });

    auto task = std::make_shared<std::packaged_task<RetType()>>(std::bind(std::forward<Func>(f),std::forward<Args>(args)...));
    // auto task = std::make_shared<std::packaged_task<retType()>>(std::move(std::bind(std::forward<Func>(f),std::forward<Args>(args)...)));

// This is because my function is a bool and we're having an eerk 05/01/22 with this vvvv
    // auto task = std::make_shared<std::packaged_task<RetType()>>(std::bind(std::forward<Func>(f),std::forward<Args>(args)...));

    {
#if __cplusplus == 201402L
      std::unique_lock<std::mutex> lock(m_jobMutex);

#elif __cplusplus == 201703L
      std::scoped_lock<std::mutex> lock(m_jobMutex);            // C++17
#endif

      //place the job into the queue
      m_jobQueue.emplace([task]() {
        (*task)();
      });
    }
    m_notifier.notify_one();

    return task->get_future();
  }

  /* utility functions */
  std::size_t getThreadCount() const {
    return m_threads.size();
  }

  private:

  using Job = std::function<void()>;
  std::vector<std::thread> m_threads;
  std::queue<Job> m_jobQueue;
  std::condition_variable m_notifier;
  std::mutex m_jobMutex;
  std::atomic<bool> m_shutdown;

  void createThreads(std::size_t numThreads)
  {
    m_threads.reserve(numThreads);
    for (int i = 0; i != numThreads; ++i)
    {
      m_threads.emplace_back(std::thread([this]()
      {                                                         // lambda starts here
        while (true)
        {
          Job job;
          {
            std::unique_lock<std::mutex> lock(m_jobMutex);
            m_notifier.wait(lock, [this] {return !m_jobQueue.empty() || m_shutdown.load(std::memory_order_relaxed); });

            if (m_shutdown.load(std::memory_order_relaxed))
            {
              break;
            }
            job = std::move(m_jobQueue.front());
            m_jobQueue.pop();
          }
          job();
        }
      }));   // ENDOF  mthreads.emplace_back lambda
    }
  }
};

#endif