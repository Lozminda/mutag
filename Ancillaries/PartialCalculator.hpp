#ifndef PARTIALCALCULATOR_HPP
#define PARTIALCALCULATOR_HPP

#include "Constants.hpp"
#include "ImageParameters.hpp"


#include <cstdint>

#include <gmp.h>

template class std::array<std::array<int, Graphics::TILE_SIZE_PXLS>, Graphics::TILE_SIZE_PXLS>;

struct partial_calculator
{
  mpf_t real, new_real, real_sqrd, temp_real, temp1, temp2;
  mpf_t cmplx, cmplx_sqrd, new_cmplx,  temp_cmplx, increment;
  mpf_t LHSide, RHSide, Top, Bottom;
  mpf_t incrementY, increment_X, startPosX;
  size_t County, Countx, thread_step, start_X;

  partial_calculator();
  void calcItrnVals(std::array<std::array<int, Graphics::TILE_SIZE_PXLS>, Graphics::TILE_SIZE_PXLS> & itrtn_vals);

  void InitialiseForThisImage(size_t sX, size_t t_s, const ImageParams & imagePs);
  bool initialised, current_calc_done;
  int sixLines, lineSplit;
  uint32_t m_maxIterations;
  uint8_t m_speed_adjY;

};

void pmfp(mpf_t param);

#endif
