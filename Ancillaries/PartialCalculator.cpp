#include "PartialCalculator.hpp"
#include "ImageParameters.hpp"

#include <array>
#include <iostream>

#include <gmp.h>

// bool partial_calculator::initialised = false;

// mpf_t partial_calculator::real;
// mpf_t partial_calculator::new_real;
// mpf_t partial_calculator::real_sqrd;
// mpf_t partial_calculator::temp_real;
// mpf_t partial_calculator::temp1;
// mpf_t partial_calculator::temp2;
// mpf_t partial_calculator::cmplx;
// mpf_t partial_calculator::cmplx_sqrd;
// mpf_t partial_calculator::new_cmplx;
// mpf_t partial_calculator:: temp_cmplx;
// mpf_t partial_calculator::increment;
// mpf_t partial_calculator::LHSide;
// mpf_t partial_calculator::RHSide;
// mpf_t partial_calculator::Top;
// mpf_t partial_calculator::Bottom;
// mpf_t partial_calculator::incrementY;
// mpf_t partial_calculator::increment_X;
// mpf_t partial_calculator::startPosX;
// size_t partial_calculator::County;
// size_t partial_calculator::Countx;
// int partial_calculator::sixLines = 0;
// int partial_calculator::lineSplit = 0;

partial_calculator::partial_calculator()
                  :initialised{false}
                  ,current_calc_done{false}
                  ,sixLines{0}
                  ,lineSplit{0}
{
  mpf_init(new_real);
  mpf_init(new_cmplx);
  mpf_init(real);
  mpf_init(cmplx);
  mpf_init(real_sqrd);
  mpf_init(cmplx_sqrd);
  mpf_init(temp_real);
  mpf_init(temp_cmplx);
  mpf_init(increment);
  mpf_init(incrementY);
  mpf_init(increment_X);
  mpf_init(startPosX);
  mpf_init(temp1);
  mpf_init(temp2);
  mpf_init(LHSide);
  mpf_init(RHSide);
  mpf_init(Top);
  mpf_init(Bottom);
}

void partial_calculator::InitialiseForThisImage(size_t sX, size_t t_s, const ImageParams & imagePs)
{
  mpf_set(increment, imagePs.MPFs.RHSide);
  mpf_set(LHSide, imagePs.MPFs.LHSide);
  mpf_set(RHSide, imagePs.MPFs.RHSide);
  mpf_set(Top, imagePs.MPFs.Top);
  mpf_set(Bottom, imagePs.MPFs.Bottom);

// because if start_X isn't zero mpf_set(a, LHSide) needs adjusting ~[103]
// increment_X needed because zoom uses increment so if i modify it, here => kaput elsewhere

  start_X = sX * imagePs.UserDefined.resolution_X;                          // for when image been done quickly
  m_speed_adjY = imagePs.UserDefined.resolution_Y;

  mpf_sub(increment, RHSide, LHSide);                           // other increments for options
  mpf_div_ui(increment, increment, Graphics::TILE_SIZE_PXLS);
  mpf_set(incrementY, increment);
  mpf_mul_ui(incrementY, incrementY, imagePs.UserDefined.resolution_Y);
  mpf_set(increment_X, increment);

  mpf_mul_ui(startPosX, increment, start_X);                    // sort out LHS starting position
  mpf_add(startPosX, startPosX, LHSide);

  m_maxIterations =  imagePs.UserDefined.maxIterations;
  thread_step = t_s * imagePs.UserDefined.resolution_X;                      //  for when image been done quickly

// Could combine these two functions but am worried about rounding errors
  mpf_mul_ui(increment_X, increment_X, thread_step);
  mpf_set(cmplx, Bottom);

  County = 0;
  Countx = start_X;
  initialised = true;
  sixLines = 0;
  lineSplit = 0;
  mpf_set(real, startPosX);
}


// Returns false when last pixel has been calculated, otherwise returns true ie
// whether the function is in the process of calculating

void partial_calculator::calcItrnVals(std::array<std::array<int, Graphics::TILE_SIZE_PXLS>, Graphics::TILE_SIZE_PXLS>& itrtn_vals)
{
// Not sure that the next 3 lines are necessary, should be initialised in IM.cpp
  if (!initialised)
  {
      throw std::runtime_error ("@ partial_calculator::calcItrnVals, initialisation hasn't occured\n");
    //InitialiseForThisImage(start_X, thread_step, imagePs);
  }
  mpf_set_ui(new_real,0);
  mpf_set_ui(new_cmplx,0);

  static const int limit = (Graphics::TILE_SIZE_PXLS / 20);

  int iteration = 0;
  bool calculating = true;
  ++sixLines;
  // if (sixLines > 6)
  // {
  //   std::cerr << char((start_X + 42));
  //   ++lineSplit;
  //   sixLines = 0;
  //   if (lineSplit == limit)
  //   {
  //     lineSplit = 0;
  //     std::cerr<<"\n"<<County<<"\n";
  //   }
  // }
  while (Countx < Graphics::TILE_SIZE_PXLS)
  {
    mpf_set(new_real, real);
    mpf_set(new_cmplx, cmplx);
    while (calculating)
    {
      mpf_mul(real_sqrd, new_real, new_real);
      mpf_mul(cmplx_sqrd, new_cmplx, new_cmplx);
      mpf_sub(temp_real, real_sqrd, cmplx_sqrd);
      mpf_add(temp_cmplx, new_real, new_real);                // Check to see if mult_ui (2) is quicker
      mpf_mul(temp_cmplx, temp_cmplx, new_cmplx);
      mpf_add(new_real, temp_real, real);
      mpf_add(new_cmplx, temp_cmplx, cmplx);
      ++iteration;
      if(iteration > m_maxIterations)
      {
        iteration = 0;                                        // iteration = 0 means black
        calculating = false;                                  // therefore in the set
      }
      mpf_add(temp1, real_sqrd, cmplx_sqrd);
      if (mpf_cmp_ui(temp1, 4) >= 0) calculating = false;
    }

    itrtn_vals[County][Countx] = iteration;
    calculating = true;
    iteration = 0;
    Countx += thread_step;
    mpf_add(real, real, increment_X);
  }                                                             //end of while countx
  current_calc_done = true;
  mpf_set(real, startPosX);
  Countx = start_X;
  County += m_speed_adjY;
  mpf_add(cmplx, cmplx, incrementY);
  if (County > (Graphics::TILE_SIZE_PXLS - 1))
  {
    County = Graphics::TILE_SIZE_PXLS - 1;
  }
  return;
}


void pmfp(mpf_t param)
{
  std::string bob = iphf::mpfToStr(param);
  return;
}