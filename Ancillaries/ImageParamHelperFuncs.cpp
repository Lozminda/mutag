#include "ImageParamHelperFuncs.hpp"

#include <cmath>
#include <cstdint>
#include <array>

// #include <gmp.h>
// // #include <gmpxx.h>

namespace iphf
{

std::string mpfToStr( mpf_t param_mpf)
{
  const uint32_t NUM_OF_DIGITS = 20;
  const uint32_t TEMP_SIZE = NUM_OF_DIGITS + 3;
  mp_exp_t exponent;

  char my_res[TEMP_SIZE];
  mpf_get_str(my_res, &exponent, 10, NUM_OF_DIGITS, param_mpf);
  --exponent;
  std::string zeros;
  bool negative = false;
  if (mpf_sgn(param_mpf) < 0) negative = true;
  std::string returnStr(my_res);

  if (negative) returnStr.erase(0, 1);                          // remove - sign
  if (exponent < 0)
  {
    exponent = -exponent;
    zeros = "0.";
    for (size_t it = 1; it < exponent; ++it)
      zeros += "0";
    returnStr = zeros + returnStr;
  }
  else
  {
    if ((exponent + 1) < returnStr.length())
    returnStr.insert((exponent + 1), 1, '.');
  }
  if (negative) returnStr = "-" + returnStr;
  if (returnStr == "0.") returnStr = "0";
  // std::cout<<"exponent at MFPtoString: "<<exponent<<
  //           " and returnStr:"<<returnStr<<"\n";
  return returnStr;
}

std::string mpfToStr( const mpf_t cparam_mpf)
{
  mpf_t param_mpf;
  mpf_init_set(param_mpf, cparam_mpf);
  const uint32_t NUM_OF_DIGITS = 20;
  const uint32_t TEMP_SIZE = NUM_OF_DIGITS + 3;
  mp_exp_t exponent;

  char my_res[TEMP_SIZE];
  mpf_get_str(my_res, &exponent, 10, NUM_OF_DIGITS, param_mpf);
  --exponent;
  std::string zeros;
  bool negative = false;
  if (mpf_sgn(param_mpf) < 0) negative = true;
  std::string returnStr(my_res);

  if (negative) returnStr.erase(0, 1);                          // remove - sign
  if (exponent < 0)
  {
    exponent = -exponent;
    zeros = "0.";
    for (size_t it = 1; it < exponent; ++it)
      zeros += "0";
    returnStr = zeros + returnStr;
  }
  else
  {
    if ((exponent + 1) < returnStr.length())
    returnStr.insert((exponent + 1), 1, '.');
  }
  if (negative) returnStr = "-" + returnStr;
  if (returnStr == "0.") returnStr = "0";
  // std::cout<<"exponent at MFPtoString: "<<exponent<<
  //           " and returnStr:"<<returnStr<<"\n";
  return returnStr;
}


void stringToMPF(mpf_t param_mpf, std::string& input)
{
  mpf_set_str ( param_mpf, (input.c_str()), 10 );
  return;
}

} // ENDOF namespace sphf
