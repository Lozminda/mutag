//Might need to write this with GMP in mind

#ifndef LETTERCOORDS_HPP
#define LETTERCOORDS_HPP

#include <cstdint>
#include <string>
#include <cstring>
#include <array>
#include <iostream>
#include <cmath>                                                // for pow()
#include <cassert>
#include <limits>

namespace ltcd
{

class LetterCoords
{

public:

  using maybeUInt64_ish = uint64_t;
  
  LetterCoords();

  LetterCoords(const char chars[]);

  ~LetterCoords() = default;

  friend bool operator==(const LetterCoords& lhs, const LetterCoords& rhs);

  friend bool operator!=(const LetterCoords& lhs, const LetterCoords& rhs);

  LetterCoords& operator++();

  LetterCoords& operator--();

  void times2();

  bool divideBy2();

  maybeUInt64_ish to_int64() const;

#ifdef DEBUG
  int mypow(int a, int b);

  template <typename RETURN_T>
  RETURN_T nlmax();
#endif

  LetterCoords& fromInt2_Letters(maybeUInt64_ish param);

// Defined in .tpp This should be LCtoPow2
  template <typename T>
  LetterCoords& PowerTwo(T level);                           // level is half again in y direction;

  // template <typename T>
  // LetterCoords& Y_boundry(T level);                           // level is half again in y direction;

  const std::string toString() const;
  LetterCoords& from_string(std::string);

  friend std::ostream & operator << (std::ostream&, const LetterCoords&);

private:

  static constexpr size_t MAXLETTERS = 6;
  static constexpr char BASE = int('z') - int('a') + 1;     // +1 Because Zero is a symbol
  std::array<char,MAXLETTERS> letters;

  LetterCoords& Dec(size_t);
  LetterCoords& Inc(size_t);

};

} // ENDOF namespace

#include "LetterCoords.tpp"
#endif