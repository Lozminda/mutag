#include "Constants.hpp"
// #include "ImageParamHelperFuncs.hpp"
#include "ImageParameters.hpp"
#include "Position.hpp"
#include "FileHandler.hpp"

#include <iostream>
#include <array>
#include <cstdint>
#include <string>
#include <cmath>

#include <gmp.h>
// #include <gmpxx.h>

// An afternoon + early evening failing to get lib gmpxx to compile we're
// stuck with the C version of the mpf_t functions, a bit ugly but it works !

bool ImageParams::ImageParamChanged = false;
bool ImageParams::maxIterationsHaveChanged = false;

UserDefined_s::UserDefined_s()
              :resolution_X{Default::RESOLUTION_X}
              ,resolution_Y{Default::RESOLUTION_Y}
              ,imageState{Default::IMAGE_STATE}
              ,maxIterations{Default::START_ITERATIONS}
{}

UserDefined_s::UserDefined_s(uint8_t resX, uint8_t resY, char state, uint32_t prec)
                    :resolution_X{resX}
                    ,resolution_Y{resY}
                    ,imageState{state}
                    ,maxIterations{prec}
{}

void UserDefined_s::Initialise(const UserDefined_s & other)
{
  resolution_X = other.resolution_X;
  resolution_Y = other.resolution_Y;
  imageState = other.imageState;
  maxIterations = other.maxIterations;
  return;
}

void UserDefined_s::toggle()
{
  if ((resolution_X == 1) && (resolution_Y == 1))
  {
    resolution_Y = 2;
    imageState = UserDefined_s::PARTIALLY_CALCULATED;
    return;
  }
  if ((resolution_X == 1) && (resolution_Y == 2))
  {
    resolution_Y = 3;
    imageState = UserDefined_s::PARTIALLY_CALCULATED;
    return;
  }
  if ((resolution_X == 1) && (resolution_Y == 3))
  {
    // resolution_X = 3;
    resolution_Y = 4;
    imageState = UserDefined_s::PARTIALLY_CALCULATED;
    return;
  }
  if ((resolution_X == 1) && (resolution_Y == 4))
  {
    // resolution_X = 3;
    resolution_Y = 10;
    imageState = UserDefined_s::PARTIALLY_CALCULATED;
    return;
  }
  if ((resolution_X == 1) && (resolution_Y == 10))
  {
    resolution_X = 1;
    resolution_Y = 1;
    imageState = UserDefined_s::COMPLETE;
    return;
  }
  return;
}

void UserDefined_s::imageStateFromResolution()
{
  if ((resolution_X == 1) && (resolution_Y == 1))
  {
    imageState = UserDefined_s::COMPLETE;
    return;
  }
  if ((resolution_X == 1) && (resolution_Y == 2))
  {
    imageState = UserDefined_s::PARTIALLY_CALCULATED;
    return;
  }
  if ((resolution_X == 1) && (resolution_Y == 3))
  {
    imageState = UserDefined_s::PARTIALLY_CALCULATED;
    return;
  }
  if ((resolution_X == 1) && (resolution_Y == 4))
  {
    imageState = UserDefined_s::PARTIALLY_CALCULATED;
    return;
  }
  if ((resolution_X == 1) && (resolution_Y == 10))
  {
    imageState = UserDefined_s::PARTIALLY_CALCULATED;
    return;
  }
  return;
}

std::ostream & operator << (std::ostream &os, const UserDefined_s & param)
{
   os<<static_cast<unsigned>(param.resolution_X)<<std::endl;
   os<<static_cast<unsigned>(param.resolution_Y)<<std::endl;
   os<<param.imageState<<std::endl;
   os<<param.maxIterations<<std::endl;
  return os;
}

MPF_s::MPF_s()
{
  mpf_init(LHSide);
  mpf_init(RHSide);
  mpf_init(Top);
  mpf_init(Bottom);
  mpf_init(ImageWidth);
  mpf_init_set_d(ImageWidth, 0.0);
  mpf_init_set_d(LHSide, Default::LEFT);
  mpf_init_set_d(RHSide, Default::RIGHT);
  mpf_init_set_d(Top, Default::TOP);
  mpf_init_set_d(Bottom, Default::BOTTOM);
}

MPF_s::MPF_s(mpf_t l, mpf_t r, mpf_t t, mpf_t b, mpf_t i)
{
  mpf_init(LHSide);
  mpf_init(RHSide);
  mpf_init(Top);
  mpf_init(Bottom);
  mpf_init(ImageWidth);
  mpf_init_set(ImageWidth, i);
  mpf_init_set(LHSide, l);
  mpf_init_set(RHSide, r);
  mpf_init_set(Top, t);
  mpf_init_set(Bottom, b);
}

MPF_s::MPF_s(signed l, signed r, signed t, signed b, signed i)
{
  mpf_init(LHSide);
  mpf_init(RHSide);
  mpf_init(Top);
  mpf_init(Bottom);
  mpf_init(ImageWidth);
  mpf_init_set_si(ImageWidth, i);
  mpf_init_set_si(LHSide, l);
  mpf_init_set_si(RHSide, r);
  mpf_init_set_si(Top, t);
  mpf_init_set_si(Bottom, b);
}

MPF_s::MPF_s(double l, double r, double t, double b, double i)
{
  mpf_init(LHSide);
  mpf_init(RHSide);
  mpf_init(Top);
  mpf_init(Bottom);
  mpf_init(ImageWidth);
  mpf_init_set_d(ImageWidth, i);
  mpf_init_set_d(LHSide, l);
  mpf_init_set_d(RHSide, r);
  mpf_init_set_d(Top, t);
  mpf_init_set_d(Bottom, b);
}

void MPF_s::Initialise(double l, double r, double t, double b, double i)
{
  // mpf_init(LHSide);
  // mpf_init(RHSide);
  // mpf_init(Top);
  // mpf_init(Bottom);
  // mpf_init(ImageWidth);
  mpf_init_set_d(ImageWidth, i);
  mpf_init_set_d(LHSide, l);
  mpf_init_set_d(RHSide, r);
  mpf_init_set_d(Top, t);
  mpf_init_set_d(Bottom, b);
  return;
}


// class Foo
// {
// public:
//     const int& get() const
//     {
//         //non-trivial work
//         return foo;
//     }

//     int& get()
//     {
//         return const_cast<int&>(const_cast<const Foo*>(this)->get());
//     }
// };


std::ostream & operator << (std::ostream &os, MPF_s & param)
{
  os<<iphf::mpfToStr((param.LHSide))<<std::endl;
  os<<iphf::mpfToStr(param.RHSide)<<std::endl;
  os<<iphf::mpfToStr(param.Top)<<std::endl;
  os<<iphf::mpfToStr(param.Bottom)<<std::endl;
  os<<iphf::mpfToStr(param.ImageWidth)<<std::endl;
  return os;
}


std::ostream & operator << (std::ostream &os, const MPF_s & param)
{
  os<<iphf::mpfToStr((param.LHSide))<<std::endl;
  os<<iphf::mpfToStr(param.RHSide)<<std::endl;
  os<<iphf::mpfToStr(param.Top)<<std::endl;
  os<<iphf::mpfToStr(param.Bottom)<<std::endl;
  os<<iphf::mpfToStr(param.ImageWidth)<<std::endl;
  return os;
}

void MPF_s::calc_ImageWidth(double width, uint64_t level)
{
  uint64_t total_ImagesPerLevel = pow(2, level - 1);
  mpf_t totIpLMPF;
  mpf_t widthMPF;
  mpf_init_set_ui(totIpLMPF, total_ImagesPerLevel);
  mpf_init_set_d(widthMPF, width);
  mpf_div(ImageWidth, widthMPF, totIpLMPF);
  return;
}

ImageParams::ImageParams()
            :MPFs()
            ,UserDefined()
{}

ImageParams::ImageParams(mpf_t l, mpf_t r, mpf_t t, mpf_t b, mpf_t i)
            :MPFs(l, r, t, b, i)
            ,UserDefined()
{}


ImageParams::ImageParams(double l, double r, double t, double b, double i)
            :MPFs(l, r, t, b, i)
            ,UserDefined()
{}

ImageParams::ImageParams(signed l, signed r, signed t, signed b, signed i)
            :MPFs(l, r, t, b, i)
            ,UserDefined()
{}


ImageParams::ImageParams(uint8_t resX, uint8_t resY, char state, uint32_t prec)
              :UserDefined(resX, resY, state, prec)
              ,MPFs()
{}

ImageParams::ImageParams(mpf_t l, mpf_t r, mpf_t t, mpf_t b, mpf_t i,
                            uint8_t resX, uint8_t resY, char state, uint32_t prec)
              :MPFs(l, r, t, b, i)
              ,UserDefined(resX, resY, state, prec)
{}

ImageParams::ImageParams(mpf_t l, mpf_t r, mpf_t t, mpf_t b, mpf_t i,
                          const UserDefined_s& user_params)
              :MPFs(l, r, t, b, i)
              ,UserDefined(user_params)
{}

ImageParams::ImageParams(double l, double r, double t, double b, double i,
                          const UserDefined_s& user_params)
                :MPFs(l, r, t, b, i)
                ,UserDefined(user_params)
{}

ImageParams::ImageParams(double l, double r, double t, double b, double i, uint8_t resX,
                          uint8_t resY, char state, uint32_t prec)
              :MPFs(l, r, t, b, i)
              ,UserDefined(resX, resY, state, prec)
{}

void ImageParams::Initialise(double l, double r, double t, double b, double i,
                          const UserDefined_s& user_params)
{
  MPFs.Initialise(l, r, t, b, i);
  UserDefined.Initialise(user_params);
  return;
}

void ImageParams::Initialise(const std::string& l, const std::string& r, const std::string& t, const std::string& b,
                  const std::string& i, const std::string& resX, const std::string& resY,
                  const std::string& state, const std::string& prec)
{
// Can very likely get rid of this as unnecessary
  mpf_set_ui(MPFs.LHSide, 0);
  mpf_set_ui(MPFs.RHSide, 0);
  mpf_set_ui(MPFs.Top, 0);
  mpf_set_ui(MPFs.Bottom, 0);
  mpf_set_ui(MPFs.ImageWidth, 0);
  mpf_set_str(MPFs.LHSide, (l.c_str()), 10);
  mpf_set_str(MPFs.RHSide, (r.c_str()), 10);
  mpf_set_str(MPFs.Top, (t.c_str()), 10);
  mpf_set_str(MPFs.Bottom, (b.c_str()), 10);
  mpf_set_str(MPFs.ImageWidth, (i.c_str()), 10);

// Can very likely get rid of this as unnecessary too
  // std::cout<<"MPFs @ ImageParams::Initialise"<<MPFs<<"\n";

  UserDefined.resolution_X = static_cast<uint8_t>(std::stoi(resX));
  UserDefined.resolution_Y = static_cast<uint8_t>(std::stoi(resY));
  UserDefined.imageState = static_cast<char>(state[0]);
  UserDefined.maxIterations = static_cast<uint32_t>(std::stol(prec));
}

std::ostream & operator << (std::ostream &os, ImageParams & param)
{
   os<<param.MPFs<<std::endl;
   os<<param.UserDefined<<std::endl;
  return os;
}

// ************************** TO DO *****************************

// PUT THESE IN IMAGEPARAMHELPERFUNCS

// *********************  END OF TO DO  *************************

ImageParams ImageParams::Load_IPs(const std::string& filename)
{
  // ImageParams returnIPs;
  if (!filename.empty())
  {
    FileHandler fh;
    auto strVec = fh.StringsFromFile(filename);
    if (!strVec.empty())
    {
      Initialise(strVec[0], strVec[1], strVec[2], strVec[3], strVec[4], strVec[5]
                          ,strVec[6], strVec[7], strVec[8]);
    }
    else std::cerr<<"File empty @ ImageParams::Load_IPs!\n";
  }
  else std::cerr<<"No filename supplied to ImageParams::Load_IPs\n";
  return *this;
}

// Might also have to pass a reference to these functions for Positon

ImageParams ImageParams::Load_IPs(const Position& position)
{
  // ImageParams returnIPs;
  std::string numberStr{std::to_string(position.fileLevel)};
  while (numberStr.length() < Default::MAX_LEVEL_DIGITS)                        //Prefixing zeros to level number
  {
    numberStr="0"+numberStr;
  }
  std::string logfilename{(Default::PATH + numberStr + (position.fileSystem_y.toString()) +
                          (position.fileSystem_x.toString()) + ".mlg")};
  FileHandler fh;
  auto strVec = fh.StringsFromFile(logfilename);
  if (!strVec.empty())
  {
    Initialise(strVec[0], strVec[1], strVec[2], strVec[3], strVec[4], strVec[5]
              ,strVec[6], strVec[7], strVec[8]);
  }
  else std::cerr<<"File empty @ ImageParams::Load_IPs!\n";
  return *this;
}
// ---------------------------------End Of todo--------------------------------
