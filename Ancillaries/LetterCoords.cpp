//Might need to write this with GMP in mind

#include "LetterCoords.hpp"

#include <string>
#include <cstring>
#include <array>
#include <iostream>
#include <cmath>                                                // for pow()
#include <cstdint>                                              // for uint_t
#include <cassert>
#include <limits>

// #define NDEBUG

namespace ltcd
{
using maybeUInt64_ish = uint64_t;

LetterCoords::LetterCoords()
{
  letters.fill('a');
}

LetterCoords::LetterCoords(const char chars[])
{
  assert (strlen(chars) == MAXLETTERS);
  for (size_t it = 0; it < MAXLETTERS; ++it)
    letters[it] = chars[(MAXLETTERS - it - 1)];
}

bool operator==(const LetterCoords& lhs, const LetterCoords& rhs)
{
  return (lhs.letters == rhs.letters);
}

bool operator!=(const LetterCoords& lhs, const LetterCoords& rhs)
{
  return !operator==(lhs,rhs);
}


LetterCoords& LetterCoords::operator++()
{
  Inc(0);
  return *this;
}

LetterCoords& LetterCoords::operator--()
{
  Dec(0);
  return *this;
}

bool LetterCoords::divideBy2()
{
  bool carry = false;
  for (size_t it = MAXLETTERS; it-- != 0;)
  {
    char temp = letters[it] - int('a');
    unsigned char result = (temp  + (carry * BASE)) / 2;
    carry = (temp + (carry * BASE)) % 2;
    letters[it] = char(result + 'a');
  }
  return carry;
}

maybeUInt64_ish LetterCoords::to_int64() const
{
  maybeUInt64_ish retval = 0;
  for (size_t it = 0; it < MAXLETTERS; ++it)
    retval += (letters[it] - char('a')) * pow(BASE,int(it));
  return retval;
}

void LetterCoords::times2()
{
  const unsigned char LAST_LETTER = MAXLETTERS - 1;
  unsigned char carryFromBefore = 0;
  for (size_t it = 0; it < MAXLETTERS; ++it)
  {
    unsigned char temp = ((letters[it] - 'a') * 2) + carryFromBefore;
    if ( temp >= BASE )
    {
      carryFromBefore = static_cast<unsigned char> (temp / BASE);
      temp = temp - (carryFromBefore * BASE);
    }
    else carryFromBefore = 0;
    if ((carryFromBefore > 0) && (it == LAST_LETTER)) throw std::runtime_error("Overflow at LetterCoords::times2\n");
    letters[it] = 'a' + char(temp);
  }
}

#ifdef DEBUG
int LetterCoords::mypow(int a, int b)
{
  return int(pow(a,b));
}
#endif

LetterCoords& LetterCoords::fromInt2_Letters(maybeUInt64_ish param)
{
// Could have sorted this with NDEBUG but can't be bothered atm
#ifdef DEBUG
  assert (param >= 0);
  assert (param < (nlmax<maybeUInt64_ish>()));
#endif
  int startPow = MAXLETTERS;
  for (size_t it = 0; it < MAXLETTERS; letters[it] = 'a', ++it);

#ifdef DEBUG
  for ( ; param < mypow(BASE, startPow); --startPow);         // Find starting power
#else
  for ( ; param < pow(BASE, startPow); --startPow);           // Find starting power
#endif

  while (startPow != - 1)
  {
#ifdef DEBUG
    int tempResult = param / mypow(BASE,startPow);
    param -= tempResult * mypow(BASE,startPow);
#else
    int tempResult = param / pow(BASE,startPow);
    param -= tempResult * pow(BASE,startPow);
#endif
    letters[startPow] = char(int('a') + tempResult);
    -- startPow;
  }
  return *this;
}

const std::string LetterCoords::toString() const                        // Backwards for file use
{
  std::string returnStr="aaaaaa";
  for (size_t letIT= 0; letIT < MAXLETTERS; ++letIT)
    returnStr.replace((MAXLETTERS - letIT - 1),1, std::string(1,letters[letIT]));
  // std::cout<<"@ LetterCoords:: returnStr: "<<returnStr<<"\n";
  return returnStr;
}

LetterCoords& LetterCoords::from_string(std::string input)     // Swapped for LetterCoord use ie
{                                                             // "aaadce" => e,c,d,a,a,a
  assert(input.length() <= 6);
  size_t inpIT = input.size();
  size_t letIT= 0;
  for (; inpIT-- != 0; ++letIT)
    letters[letIT] = input[inpIT];
  return *this;
}

std::ostream& operator << (std::ostream & os, const LetterCoords &var)
{
  for (size_t it = var.letters.size(); it-- != 0;)
  {
    os<<var.letters[it];
  }
  return os;
}

// ------------------- PRIVATE -------------------

LetterCoords& LetterCoords::Inc(size_t Marker)
{
  if (letters[Marker]!='z') ++letters[Marker];
  else
  {
    letters[Marker]='a';
    Inc(Marker+1);
  }
  return *this;
}

LetterCoords& LetterCoords::Dec(size_t Marker)
{
  if (letters[Marker]!='a') --letters[Marker];
  else
  {
    letters[Marker]='z';
    Dec(Marker+1);
  }
  return *this;
}

} // ENDOF namespace
