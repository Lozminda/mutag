#ifndef DELETERTEMPLATES_TPP
#define DELETERTEMPLATES_TPP

#ifndef DELETERTEMPLATES_HPP
#error __FILE__ should only be included from LetterCoords.hpp.
#endif // LetterCoords_HPP


#include <memory>

// inline
template <typename Object, void (*DeleterFun)(Object*)>
struct Deleter
{
  void operator() (Object* obj) const noexcept
  {
      try {
          DeleterFun(obj);
      } catch (...) {
          // Handle error
      }
  }
};
// inline
template <typename Object, void (*DeleterFun)(Object*)>
using UniquePtr = std::unique_ptr<Object, Deleter<Object, DeleterFun>>;

// inline
template <typename Object>
using SharedPtr = std::shared_ptr<Object>;

#endif