
#include"GTexture.hpp"

#include <iostream>
#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

namespace hsdl
{

using Unique_SDL_Surface = UniquePtr<SDL_Surface, SDL_FreeSurface>;
using Shrd_SDL_Txtr = SharedPtr<SDL_Texture>;                 // See DeleterTemplates.hpp


GTexture::GTexture()
  :width{0},
  height{0},
  texture{nullptr},
  sourceRect({0, 0, 0, 0}),
  destRect({0, 0, 0, 0}),
  destX{0},                                                // NOT SURE THESE SHOULD BE IN{0},
  destY{0},
  destW{0},
  destH{0},
  sourceX{0},
  sourceY{0},
  sourceW{0},
  sourceH{0}
{}

bool GTexture::CreateTextureFromPNG(SDL_Renderer* renderer, const std::string& name)
{
  bool success = false;
  Unique_SDL_Surface loadedSurface{ IMG_Load(name.c_str()) };
  if (loadedSurface == nullptr) std::cerr<<"Unable to load image,  surface is NULL "<<name<<" @ GTexture::Load_Tex\n";
  else
  {
    texture = Shrd_SDL_Txtr{SDL_CreateTextureFromSurface( renderer, loadedSurface.get() ),
                              [](SDL_Texture *ptr){SDL_DestroyTexture(ptr);}};

    if (texture == nullptr) std::cerr<<"Unable to create texture in Load_TexGTxtr ";
    else
    {
      success = true;
      width = loadedSurface->w;
      height = loadedSurface->h;
    }
  }
  return success;
}

void GTexture::Render(SDL_Renderer* renderer, int x, int y,
                      int w, int h, bool Is_Flipped)
{
  SDL_RendererFlip flip = SDL_FLIP_NONE;
  if (Is_Flipped) flip = SDL_FLIP_VERTICAL;

// The sourceRect is set outside this function whereas the dest quad is here. It's a bit
// weird maybe I need to look at the API design some more,  just hhhhmmm
  // sourceRect = {0, 0, sourceW, sourceH};
  if (w == 0) w = width;
  if (h == 0) h = height;

  sourceRect = {0, 0, width, height};
  destRect = {x , y, w, h};                         //  There might be some FLIPPING later
  SDL_RenderCopyEx( renderer, texture.get(),  &sourceRect, &destRect, 0, nullptr, flip);
  // SDL_RenderCopyEx( renderer, texture. get(),  nullptr, nullptr, 0, nullptr, flip);
  return;
}

// Set source rectangle before Rendering: THIS MAY NEED FURTHER WORK ...

  void GTexture::RenderSrcRct(SDL_Renderer* renderer, int x, int y,
                        int w, int h, bool Is_Flipped)
  {
    SDL_RendererFlip flip = SDL_FLIP_NONE;
    if (Is_Flipped) flip = SDL_FLIP_VERTICAL;

// The sourceRect is set outside this function whereas the dest quad is here. It's a bit
// weird maybe I need to look at the API design some more,  just hhhhmmm
    destRect = {x , y, w, h};                         //  There might be some FLIPPING later
    SDL_RenderCopyEx( renderer, texture.get(),  &sourceRect, &destRect, 0, nullptr, flip);
    return;
  }

bool GTexture::Assign1_Col(SDL_Renderer* renderer,  uint8_t R,  uint8_t G, uint8_t B,
                        int wdth = 0, int hght = 0)
{
  width = wdth;
  height = hght;

  SDL_Rect rect = {0, 0, width, height};
// -------------- CHANGE THIS COMMENT -------------- 
// All this gubbins sets the texture to a square of whatever colour is saved
// in the image file that's actaully a text file that says "All One Colour:"
// (or "NoI:") followed by some number. Usually 0. So when the user is in the
//  actual set they just get a black square (or square of some other uniform
// colour)

  texture = Shrd_SDL_Txtr{ SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888,
             SDL_TEXTUREACCESS_TARGET, width, height),
              [](SDL_Texture *ptr){SDL_DestroyTexture(ptr);} };
  SDL_SetRenderTarget(renderer, texture.get());
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  SDL_RenderClear(renderer);
  SDL_SetRenderDrawColor(renderer, R, G, B, 255);
  SDL_RenderDrawRect(renderer, &rect);                             // prob one of them can go
  SDL_RenderFillRect(renderer, &rect);
  Render(renderer, 0, 0, width, height);
  SDL_SetRenderTarget(renderer, nullptr);
  return (texture != nullptr);
}

void GTexture::PrepareToBeRendered_To(SDL_Renderer* renderer, int w, int h)
{
  texture = Shrd_SDL_Txtr{ SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888,
              /*SDL_TEXTUREACCESS_STREAMING*/SDL_TEXTUREACCESS_TARGET, w, h),
                [](SDL_Texture *ptr){SDL_DestroyTexture(ptr);} };
  SDL_SetRenderTarget(renderer,texture.get());
  SDL_SetRenderDrawColor(renderer,0,0,0,255);
  SDL_RenderClear(renderer);
  width = w;
  height = h;
  return;
}


// Not sure whether returning false is so good,  perhaps should throw. Not performance critical
// NB Destroys texture in the process of saving !
bool GTexture::SaveAsPNG(SDL_Renderer* renderer, const std::string & filename, int width,
                          int height)
{
  bool success = true;
  if (texture == nullptr)
  {
    std::cerr<<"ImageTexture is NULL @ GTexture::SaveAsPNG !!! Could this be the error ?\n";
    success = false;
  }
  else
  {
    Unique_SDL_Surface surface{SDL_CreateRGBSurface(0, width, height, 32, 0, 0, 0, 0)};
    if (surface == nullptr) success = false;
    else
    {
      SDL_RenderReadPixels(renderer, NULL, surface->format->format, surface->pixels, surface->pitch);
      IMG_SavePNG(surface.get(), filename.c_str());
      SDL_SetRenderTarget(renderer,texture.get());
      texture = nullptr;
    }
  }
  return success;
}

void GTexture::Set_sourceRect(int sx, int sy, int sw, int sh)
{
  sourceX = sx;
  sourceY = sy;
  sourceW = sw;
  sourceH = sh;
  sourceRect = {sx, sy, sw, sh};
  return;
}

void GTexture::Set_destRect(int dx, int dy, int dw, int dh)
{
  destX = dx;
  destY = dy;
  destW = dw;
  destH = dh;
  destRect = {dx, dy, dw, dh};
  return;
}

Shrd_SDL_Txtr GTexture::GetTexture()
{
  return texture;
}

int GTexture::GetWidth() {return width;}

int GTexture::GetHeight() {return height;}

}   // ENDOF namespace