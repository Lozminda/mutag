#ifndef GTEXTURE_H
#define GTEXTURE_H

#include "DeleterTemplates.hpp"
// #define NDEBUG
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
// #include <SDL2/SDL_ttf.h>
#include <string>

namespace hsdl
{

using Shrd_SDL_Txtr = SharedPtr<SDL_Texture>;                 // See DeleterTemplates.hpp

class GTexture
{
  public:
    int width;
    int height;

    GTexture();
    ~GTexture() = default;


// Loads a .png image by the name which is passed through std::string
// and converts that to a texture stored in 'texture' returns true
// if successful.

    bool CreateTextureFromPNG(SDL_Renderer*, const std::string &);


// Sets the RGB colour for text to be written

    // void Set_WritingColour(Uint8 red, Uint8 green, Uint8 blue);

// Assigns "writing" to texture (to then be rendered)
    // bool CreateWrittenWord(SDL_Renderer* renderer, const std::string& writing);

// Assigns font_ptr
    // void Load_font(const std::string& font_name, int font_size);

// Assigns a RGB colour to texture of width and height. Returns true if
// successful.

    bool Assign1_Col(SDL_Renderer*, uint8_t ,  uint8_t,  uint8_t, int, int);


// Renders at x, y,  same dimensions as the texture rendered to a rectangle of size w, h.
// If true is passed then the image will be flipped.

    void Render(SDL_Renderer* renderer, int x, int y, int w = 0, int h = 0,
                bool Is_Flipped = false);


// Renders at x, y; a rectangle of size w, h. SourceRectangle should be specified before use
// If true is passed then the image will be flipped

    void RenderSrcRct(SDL_Renderer* renderer, int x, int y, int w, int h, bool Is_Flipped = false);


// Sets texture as the rendering target so that other textures can be
// rendered to it

    void PrepareToBeRendered_To(SDL_Renderer*, int width, int height);


// Saves texture as a PNG named 'filename'. Returns false if it fails.
// The texture will destoyed afterwards

    bool SaveAsPNG(SDL_Renderer* renderer, const std::string & filename, int width,
                    int height);


// I'm having my doubts about the last three,  should they even be implemented ?

    void Set_sourceRect(int x, int y, int width, int height);

    void Set_destRect(int x, int y, int width, int height);

    Shrd_SDL_Txtr GetTexture();

    int GetWidth();

    int GetHeight();

    // void kill();

  protected:

    Shrd_SDL_Txtr texture;
    SDL_Rect sourceRect;
    SDL_Rect destRect;

    int destX;                                                // NOT SURE THESE SHOULD BE INT
    int destY;
    int destW;
    int destH;
    int sourceX;
    int sourceY;
    int sourceW;
    int sourceH;
};

}   // ENDOF namespace

#endif