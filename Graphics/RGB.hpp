#ifndef _RGBClass_
#define _RGBClass_

 // #define NDEBUG

#include <vector>
#include <stdint.h>

struct RGB
{
    RGB() {}
    template<typename T>

    void assignValues(const T & max_itrtns_pxl);

    std::vector<uint8_t> Red;
    std::vector<uint8_t> Green;
    std::vector<uint8_t> Blue;

};
#include "RGB.tpp"
#endif