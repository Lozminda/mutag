#ifndef SDL_RAII_HPP
#define SDL_RAII_HPP

struct SDL_RAII
{
    SDL_RAII();
    ~SDL_RAII();

    SDL_RAII(const SDL_RAII&) = delete;
    SDL_RAII(SDL_RAII&&) = delete;
    SDL_RAII& operator=(const SDL_RAII&) = delete;
    SDL_RAII& operator=(SDL_RAII&&) = delete;
};
#endif