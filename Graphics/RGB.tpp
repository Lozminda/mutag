#include <vector>
#include <cstdint>

// template class std::vector<uint16_t>;
// // template class std::vector<uint32_t>;
// // template class std::vector<uint64_t>;
// template class std::vector<unsigned long long>;
// template class std::vector<int>;
// template class std::vector<unsigned>;
// template class std::vector<unsigned long>;

// For debugging in gdb for operator [] et al. _vector_base
// indicates what is the base type ie unsigned char.
template class std::vector<unsigned char>;

template<typename T>
void RGB::assignValues(const T & max_itrtns_pxl)
{
  const uint8_t WIGGLE = 2;
// All these numbers just happen to give the colour scheme I like
// after much trial and error I like these colours, gives the low
// iterations a nice orange! 225,150,20:203,215,19
  Red.clear();
  Green.clear();
  Blue.clear();
  for(uint64_t i = 0;i<max_itrtns_pxl + WIGGLE;++i)
  {
    Red.push_back( (i + 199) % 255);
    Green.push_back( (149 - (i * 4)) % 255);
    Blue.push_back( (i + 83) % 255);
  }
  Red[0] = 0;
  Green[0] = 0;
  Blue[0] = 0;
  return;
}
