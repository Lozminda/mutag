#include "StringTextures.hpp"
#include "Position.hpp"
#include "FileHandler.hpp"

#include <array>

SDL_Rect StringTextures::information_VP={ information_VP.x = 2,
                                          information_VP.y = 2,    // (688-680)/2
                                          information_VP.w = 279,  // 282 - 5
                                          information_VP.h = 682 };

void StringTextures::Render(SDL_Renderer* renderer, const Position& position, ImageParams& imagePs,
                            Information_options info_options)
{
  SDL_RenderSetViewport(renderer, &StringTextures::information_VP );
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  SDL_RenderFillRect(renderer, &StringTextures::information_VP);
  if (info_options == co_ordinates) co_ord_InfoTextureCreate(renderer, position, imagePs);
  infoTextures[info_options].RenderSrcRct(renderer, Default::frameThickness, 287
                                          ,information_VP.w - Default::FRAME_THICKNESS
                                          ,information_VP.h - Default::FRAME_THICKNESS, false);
}

void StringTextures::SetUp(SDL_Renderer* renderer)
{
  infoStrings[instructions].assign("Help/How to Use*_"
                                   "  *_"
                                   "MUTAG has two modes: Calculation and*_"
                                   "Navigation. Calculation mode can be an*_"
                                   "automatic process whereas Navigation*_"
                                   "is User controlled. Calculation detail*_"
                                   "user defined in unexplored parts of*_"
                                   "the set*_");

  infoStrings[co_ordinates].assign("Image information*_"
                                   "  *_"
                                   "This is going to be more complicated*_"
                                   "THIS STRING WILL BE ASSIGNED LATER*_"
                                   "THIS IS JUST SOME 'HOLDING' INFORMATION*_");

  infoStrings[keyBindings].assign("Keyboard Controls*_"
                                  "  *_"
                                  "*&ESCAPE KEY         quit*_"
                                  "*&SPACE BAR            change % of image*_"
                                  "*&                               drawn: 10,25,50,100%*_"
                                  "*&ARROW KEYS        up, down, left, right*_"
                                  "*&q                             speed up*_"
                                  "*&a                             speed down*_"
                                  "*&d                             set speed to default*_"
                                  "*&-                              zoom out*_"
                                  "*&=                             zoom in*_"
                                  "*&m                             max iterations * 2*_"
                                  "*&k                             max iterations = 1000*_"
                                  "*&n                             max iterations - 250*_"
                                  "*&i                              toggle informaton*_");
  infoStrings[mouse].       assign("Mouse Controls*_"
                                   "  *_"
                                   "*&Left mouse button held down:*_"
                                   "move in that direction away from the centre*_"
                                   "at a speed proportional to distance from the*_"
                                   "middle I.e. If the mouse is in the bottom right*_"
                                   "corner of the Mandelbrot the direction of*_"
                                   "travel will be down and to the right quickly.*_"
                                   "Near the centre the motion is much slower*_"
                                   "  *_"
                                   "*&Right mouse button:*_"
                                   "A click takes the location under the cross*_"
                                   "and moves it to the centre*_"
                                   "  *_"
                                   "*&Mouse wheel:*_"
                                   "Zooms in and out*_");
  // information_VP.x = 2;
  // information_VP.y = 2;    // (688-680)/2
  // information_VP.w = 279;  // 282 - 5
  // information_VP.h = 682;  // 688 - 6?;
  setUp_InfoTextures(renderer);
}

void StringTextures::setUp_InfoTextures(SDL_Renderer* renderer)
{
  const size_t tokenLength = std::string("*_").length();
  for (size_t info = Information_options::keyBindings;         // See InfoToggleEnum.hpp
        info < Information_options::FINAL; ++info)
  {
    bool firstLine = true;
    size_t pos = 0;
    size_t newPos = 0;
    infoTextures[info].PrepareToBeRendered_To(renderer,
                                              information_VP.w - Default::FRAME_THICKNESS,
                                              information_VP.h - Default::FRAME_THICKNESS);

// Must also use Set_sourceRect() when Preping to be rendered too..
    infoTextures[info].Set_sourceRect(0, 0, information_VP.w - Default::FRAME_THICKNESS,
                                      information_VP.h - Default::FRAME_THICKNESS);
    int y = /*information_VP.y + 2*/0;
    int x = /*information_VP.x + 2*/0;                                                    // width of frame
    while (pos != infoStrings[info].length())
    {
      newPos = infoStrings[info].find("*_", pos);
      std::string temp = infoStrings[info].substr(pos, (newPos - pos));
      bool left_justified = false;
      if (temp != "")
      {
        if (temp.find("*&", 0) == 0) left_justified = true;
        hsdl::GWriting aLineTexture;
        if ( ! aLineTexture.Load_font("/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf", 13) )
          std::cerr<<"Failed to load font for infoTextures["<<info
            <<"] at SDLEnvironment::arrangeWritingOntoTexture()";
        else
        {
          if (firstLine)
          {
            aLineTexture.Set_WritingColour(230, 230, 230);
            firstLine = false;
          }
          else aLineTexture.Set_WritingColour(150, 150, 150);

          if (left_justified) temp.erase(0, tokenLength);       //Don't want the '*&' at beginning

          aLineTexture.CreateWrittenWord(renderer, temp);
          if (aLineTexture.GetWidth() <= information_VP.w)
          {
            if (left_justified)
            {
              aLineTexture.Render(renderer, x /*+ FRAME_THICKNESS*/, y);
            }
            else
            {
              aLineTexture.Render(renderer
                                  ,x + ((information_VP.w - aLineTexture.GetWidth()) / 2)
                                  ,y);
            }
            y += aLineTexture.GetHeight() + 1;
            if (y > (information_VP.y + information_VP.h))
            {
              std::cerr<<temp<<"\n";
              throw std::runtime_error ("Too many lines of text @ arrangeWritingOntoTexture()\n");
            }
          }
          else
          {
            std::cerr<<"error string:"<<temp<<"\n";
            throw std::runtime_error("std::string too long between *_ tokens @ SDLEnvironment"
                                      "::arrangeWritingOntoTexture()\n");
          }
        }   // ENDOF else from fontLoad
      }    // ENDOF if (temp != "")

      pos = newPos + tokenLength;                               // lenght("*_")
    }   // ENDOF while (pos != std::string::npos)
  }   // ENDOF for loop
  SDL_SetRenderTarget(renderer, nullptr);
  return;
}


void StringTextures::co_ord_InfoTextureCreate(SDL_Renderer* renderer, const Position& position, ImageParams& imagePs)
{
  if (ImageParams::ImageParamChanged)
  {
    std::cerr<<"Co-ord Info Texture is being changed\n";
    const size_t tokenLength = std::string("*_").length();
    bool firstLine = true;
    size_t pos = 0;
    size_t newPos = 0;
    infoTextures[Information_options::co_ordinates].PrepareToBeRendered_To(renderer,
                                              information_VP.w - Default::FRAME_THICKNESS,
                                              information_VP.h - Default::FRAME_THICKNESS);

  // Must also use Set_sourceRect() when Preping to be rendered too..
    infoTextures[Information_options::co_ordinates].Set_sourceRect(0, 0, information_VP.w - Default::FRAME_THICKNESS,
                                      information_VP.h - Default::FRAME_THICKNESS);
    int y = 0;
    int x = 0;                                                    // width of frame
    hsdl::GWriting aLineTexture;
    if ( ! aLineTexture.Load_font("/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf", 13) )
      std::cerr<<"Failed to load font @co_ord_InfoTextureCreate()\n";
    else
    {
      std::string numberStr{std::to_string(position.fileLevel)};
      while (numberStr.length() < Default::MAX_LEVEL_DIGITS)                        //Prefixing zeros to level number
      {
        numberStr="0"+numberStr;
      }
      std::string logfilename{(Default::PATH + numberStr + (position.fileSystem_y.toString()) +
                              (position.fileSystem_x.toString()) + ".mlg")};
      FileHandler fh;
      auto strVec = fh.StringsFromFile(logfilename);

      std::array<std::string, 15> lineOf_text;
      lineOf_text[0] = "                   Image Information";
      lineOf_text[1] = "Top left:";

// file format is in left, right, top, bottom
// IF BELOW X AXIS SWAP lineOf_text[3] AND [7] strVec[2&3]

      // lineOf_text[2] = iphf::mpfToStr(imagePs.MPFs.LHSide);       //Don't want the '*&' at beginning
      lineOf_text[2] = "X: " + strVec[0];
      // lineOf_text[3] = iphf::mpfToStr(imagePs.MPFs.Top);
      if (position.small_TextureBelowXaxis)
      {
        lineOf_text[3] = "Y: -" + strVec[3];
        lineOf_text[7] = "Y: -" + strVec[2];
      }
      else
      {
        lineOf_text[3] = "Y: " + strVec[2];
        lineOf_text[7] = "Y: " + strVec[3];
      }
      lineOf_text[4] = " ";
      lineOf_text[5] = "Bottom right: ";
      // lineOf_text[6] = iphf::mpfToStr(imagePs.MPFs.RHSide);
      lineOf_text[6] = "X: " + strVec[1];
      // lineOf_text[7] = iphf::mpfToStr(imagePs.MPFs.Bottom);
      lineOf_text[8] = " ";
      lineOf_text[9] = "Percentage of image completion";
      if (imagePs.UserDefined.resolution_X == 1 && imagePs.UserDefined.resolution_Y == 1)
        lineOf_text[10] = "100%";
      else if (imagePs.UserDefined.resolution_X == 1 && imagePs.UserDefined.resolution_Y == 2)
        lineOf_text[10] = "50%";
      else if (imagePs.UserDefined.resolution_X == 1 && imagePs.UserDefined.resolution_Y == 3)
        lineOf_text[10] = "33%";
      else if (imagePs.UserDefined.resolution_X == 1 && imagePs.UserDefined.resolution_Y == 4)
        lineOf_text[10] = "25%";
      else if (imagePs.UserDefined.resolution_X == 1 && imagePs.UserDefined.resolution_Y == 10)
        lineOf_text[10] = "10%";
      lineOf_text[11] = " ";
      lineOf_text[12] = "Max number of iterations per pixel: ";
      lineOf_text[13] = std::to_string(imagePs.UserDefined.maxIterations);
      lineOf_text[14] = " ";
      for (size_t it = 0; it < lineOf_text.size(); ++it)
      {

        if (firstLine)
        {
          aLineTexture.Set_WritingColour(230, 230, 230);
        }
        else aLineTexture.Set_WritingColour(150, 150, 150);
        aLineTexture.CreateWrittenWord(renderer, lineOf_text[it]);
        if (aLineTexture.GetWidth() <= information_VP.w)
        {
          aLineTexture.Render(renderer, x /*+ FRAME_THICKNESS*/, y);
          y += aLineTexture.GetHeight() + 1;
          if (firstLine)
          {
            y += aLineTexture.GetHeight() + 1;
            firstLine = false;
          }
          if (y > (information_VP.y + information_VP.h))
          {
            std::cerr<<lineOf_text[it]<<"\n";
            throw std::runtime_error ("Too many lines of text @ co_ord_InfoTextureCreate()\n");
          }
        }   // ENDOF if (aLineTexture.GetWidth() <= information_VP.w)
        else
        {
          std::cerr<<"error string:"<<lineOf_text[it]<<"\n";
          throw std::runtime_error("std::string too long between *_ tokens @ SDLEnvironment"
            "::co_ord_InfoTextureCreate()\n");
        }
      }   // ENDOF for (size_t it = 0; it < lineOf_text.size(); ++it)
    }   // ENDOF has fontloaded ok ?
  }   // ENDOF if (ImageParams::ImageParamChanged)
  ImageParams::ImageParamChanged = false;
  SDL_SetRenderTarget(renderer, nullptr);
  return;
}

std::string StringTextures::PIT(size_t index)
{
  return infoStrings[index];
}
