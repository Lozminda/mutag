#include "Constants.hpp"
#include "SmallTexture.hpp"
#include "Position.hpp"
#include "ImageParameters.hpp"
#include "FileHandler.hpp"

#include <string>
#include <sstream>
#include <regex>

#include <gmp.h>

SmallTexture::SmallTexture()
            // :IPs_alreadySet{false}
{
  mpf_set_default_prec(128);
  mpf_init(MPF_LEFT);
  mpf_init(MPF_TOP);
  mpf_init_set_d(MPF_LEFT, Default::LEFT);
  mpf_init_set_d(MPF_TOP, Default::TOP);
}

void SmallTexture::ResetFileNFlips()
{
  filename="000000aaaaaaaaaaaa.mbt";
  flipped=false;
  return;
}

void SmallTexture::Calc_MPFs(uint64_t level)
{
  imagePs.MPFs.calc_ImageWidth((Default::RIGHT - Default::LEFT), level);
  Position tempForCalc;
  SplitFilenameIntoPosMembers(tempForCalc.fileLevel, tempForCalc.fileSystem_x,
                               tempForCalc.fileSystem_y);
  if (tempForCalc.fileLevel != level)
    std::cerr<<"Error at SmallTexture::Calc_MPFs() level from"
                "SplitFilenameIntoPosMembers and level (as param) not the same !\n";

  uint64_t filePosDis = tempForCalc.fileSystem_x.to_int64();
  // std::cout<<"filePosDis: "<<filePosDis<<"\n";
// Left & Right
  mpf_mul_ui(imagePs.MPFs.LHSide, imagePs.MPFs.ImageWidth, filePosDis);
  mpf_add(imagePs.MPFs.LHSide, imagePs.MPFs.LHSide, MPF_LEFT);
  mpf_add(imagePs.MPFs.RHSide, imagePs.MPFs.LHSide, imagePs.MPFs.ImageWidth);
// Top and Bottom
  filePosDis = tempForCalc.fileSystem_y.to_int64();
  mpf_mul_ui(imagePs.MPFs.Top, imagePs.MPFs.ImageWidth, filePosDis);
  mpf_sub(imagePs.MPFs.Top, MPF_TOP, imagePs.MPFs.Top);
  mpf_sub(imagePs.MPFs.Bottom, imagePs.MPFs.Top, imagePs.MPFs.ImageWidth);
  // std::cout<<"(@Calc_MPFs) Left Right Top and Bottom MPF for this texture\n";
  // std::cout<<"spMPF: "<<spMPF<<"\n";
  return;
}

// void SmallTexture::SplitFilenameIntoPosMembers(std::string filename, uint64_t & lvl,
//                                                       ^^^^^^^^^^^^^
//                                                       smallTxtrArr[index].filename
void SmallTexture::SplitFilenameIntoPosMembers(uint64_t & lvl, ltcd::LetterCoords & fPx,
                                                ltcd::LetterCoords & fPy)
{
  std::string temp{filename};
  temp.erase(0, std::string(Default::PATH).length());
  std::string numberStr = temp.substr(0, 6);
  std::istringstream iss(numberStr);
  iss >> lvl;
  fPy.from_string(temp.substr(6, 6));
  fPx.from_string(temp.substr(12, 6));
  return;
}

bool SmallTexture::Load_OneColourInfo(uint64_t& oneColourOrItrtn)
{
  FileHandler fh;
  std::vector<std::string> contents{fh.StringsFromFile(filename)};
  if (contents.empty())
  {
    std::cerr<<"Nothing in file:"<<filename<<
              " @ load_Blackscreen()\n";
    return false;
  }
  for(const auto &a_line  : contents)
  {
    if (a_line.length() > 1)
    {
      std::string output = std::regex_replace(a_line,
        std::regex("[^0-9]*([0-9]+).*"),std::string("$1"));
      oneColourOrItrtn = std::stoi(output);
    }
  }
  return true;
}
