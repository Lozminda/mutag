#include "SDL_RAII.hpp"

#include <exception>
#include <stdexcept>
// #include <memory>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>


SDL_RAII::SDL_RAII()
{

// combine SLD_Error with std exception !

  if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
  {
    throw std::runtime_error("SDL could not initialize! SDL Error:" + std::string(SDL_GetError()) + "\n");
  }
  else if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
  {
    throw std::runtime_error("Warning: Linear texture filtering not enabled!" + std::string(SDL_GetError()) + "\n");
  }
  int imgFlags = IMG_INIT_PNG;
  if (!(IMG_Init(imgFlags) & imgFlags))
  {
    throw std::runtime_error("SDL_image could not initialize!" + std::string(SDL_GetError()) + "\n");
  }
  if(TTF_Init() == -1)
  {
      throw std::runtime_error("SDL_ttf could not initialize!" + std::string(TTF_GetError()) + "\n");
  }

}

SDL_RAII::~SDL_RAII()
{
// Should this be in a try block what errors does SDL_Quit throw
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
}
