#include "GWriting.hpp"
#include <iostream>

namespace hsdl
{

using Unique_SDL_Surface = UniquePtr<SDL_Surface, SDL_FreeSurface>;

GWriting::GWriting()
        :textColour({0, 0, 0})
        ,font_ptr{nullptr}
{}

void GWriting::Set_WritingColour(Uint8 red, Uint8 green, Uint8 blue)
{
  textColour = {red, green, blue};
}

bool GWriting::Load_font(const std::string& font_name, int font_size)
{
  font_ptr = TTF_OpenFont(font_name.c_str(), font_size);
  if (font_ptr == nullptr){
   std::cerr<<"AArk can't load font (@GWriting::Load_font) "<<font_name<<
              ". Error:"<<TTF_GetError()<<" !\n";
  }
  TTF_SetFontKerning(font_ptr, false);
  return (font_ptr != nullptr);
}

bool GWriting::CreateWrittenWord(SDL_Renderer* renderer, const std::string& writing)
{

  // Unique_SDL_Surface textSurface{TTF_RenderText_Blended(font_ptr, writing.c_str(), textColour )};
  Unique_SDL_Surface textSurface{TTF_RenderText_Solid(font_ptr, writing.c_str(), textColour )};
  if (textSurface == nullptr)
    std::cerr<<"Unable to render text surface! SDL_ttf Error:"<<std::string(TTF_GetError());
  else
  {
    texture = Shrd_SDL_Txtr{SDL_CreateTextureFromSurface(renderer, textSurface.get()),
                            [](SDL_Texture *ptr){SDL_DestroyTexture(ptr);}};
    if( texture == nullptr)
      std::cerr<<"Unable to create texture from rendered text! SDL Error:"<<
        std::string(SDL_GetError())<<"\n";
    else
    {
      width = textSurface->w;
      height = textSurface->h;
    }
  }
  return texture != nullptr;
}


// Load_Font first before using this function ..

std::string GWriting::FitStringInPxl_Width(SDL_Renderer* renderer, const std::string & writing, uint16_t width,
                                      const std::string & token)
{
  size_t pos = std::string::npos;
  std::string partial_string;
  while (pos != 0)
  {
    int w, h;
    partial_string = writing.substr(0, pos);
    TTF_SizeText(font_ptr, partial_string.c_str() , &w, &h);
    if (w <= width)
    {
      CreateWrittenWord(renderer, partial_string);
      return pos == std::string::npos ? std::string() :
                      writing.substr(pos, (writing.length() - 1));
    }
    pos = writing.rfind(token, pos - 1);                        // rfind returns npos if no tokens
    if (pos == std::string::npos) pos = 0;                      // no more tokens, end while
  }
  std::cerr<<"Input string too large at GWriting::FitStringInPxl_Width() "
            <<partial_string<<"\n";
  return writing;
}

}   // ENDOF namespace hsdl