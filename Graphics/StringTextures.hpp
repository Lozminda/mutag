#ifndef STRINGTEXTURES_HPP
#define STRINGTEXTURES_HPP
// #define NDEBUG

#include "Constants.hpp"
#include "InfoToggleEnum.hpp"
#include "GWriting.hpp"
#include "Position.hpp"
#include "ImageParameters.hpp"

#include <SDL2/SDL.h>
// #include <SDL2/SDL_image.h>
// #include <SDL2/SDL_ttf.h>

#include <array>
#include <string>

class StringTextures
{
  public:
    StringTextures() = default;
    ~StringTextures() = default;

    StringTextures(const StringTextures&) = delete;
    StringTextures & operator=(const StringTextures&) = delete;
    StringTextures(StringTextures&&) = delete;
    StringTextures& operator=(StringTextures&&) = delete;

    void Render(SDL_Renderer* renderer, const Position& position, ImageParams& imagePs,
                Information_options);

    void SetUp(SDL_Renderer*);

    static SDL_Rect information_VP;

  private:
    std::array<hsdl::GWriting, 4> infoTextures;
    std::array<std::string, 4> infoStrings;

// SHOULD BE PASSING imageparams NOT POSITION ?

    void setUp_InfoTextures(SDL_Renderer*);
    void createBlankTexture(SDL_Renderer* renderer);
    void co_ord_InfoTextureCreate(SDL_Renderer*, const Position& position, ImageParams& imagePs);
    std::string PIT(size_t index);


};

#endif