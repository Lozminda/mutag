#ifndef BLANKTEXTURE_HPP
#define BLANKTEXTURE_HPP

#include "GWriting.hpp"
#include <SDL2/SDL.h>

struct BlankTexture
{
  public:
    BlankTexture() = default;
    ~BlankTexture() = default;
    
    BlankTexture(const BlankTexture&) = delete;
    BlankTexture & operator=(const BlankTexture&) = delete;
    BlankTexture(BlankTexture&&) = delete;
    BlankTexture& operator=(BlankTexture&&) = delete;

    void createBlankTexture(SDL_Renderer* renderer);
    hsdl::GWriting GWTexture;
};

#endif