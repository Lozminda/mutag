#include "Constants.hpp"
#include "BlankTexture.hpp"
#include "GWriting.hpp"
#include <iostream>

// BlankTexture::BlankTexture(SDL_Renderer* renderer)
// {
//   createBlankTexture(renderer);
// }

void BlankTexture::createBlankTexture(SDL_Renderer* renderer)
{
  GWTexture.PrepareToBeRendered_To(renderer, Graphics::TILE_SIZE_PXLS, Graphics::TILE_SIZE_PXLS);

  // Must also use Set_sourceRect() when Preping to be rendered too..
  GWTexture.Set_sourceRect(0, 0, Graphics::TILE_SIZE_PXLS, Graphics::TILE_SIZE_PXLS);
  std::string message = "This image currently does not exist; it's being calculated";
  hsdl::GWriting aLineTexture;
  if ( ! aLineTexture.Load_font("/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf", 24) )
    std::cerr<<"Failed to load font for BlankTexture @ StringTextures::createBlankTexture()";
  else
  {
    aLineTexture.Set_WritingColour(230, 230, 230);

    aLineTexture.CreateWrittenWord(renderer, message);
    if (aLineTexture.GetWidth() <= Graphics::TILE_SIZE_PXLS)
    {
      aLineTexture.Render(renderer, ( (Graphics::TILE_SIZE_PXLS - aLineTexture.GetWidth()) / 2 ),
                          ( (Graphics::TILE_SIZE_PXLS) / 2 ) ) ;
    }
    else throw std::runtime_error ("BlankTexture FUBAR !\n");
  }
  SDL_SetRenderTarget(renderer, nullptr);
  return;
}
