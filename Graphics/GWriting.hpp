#ifndef GWRITING_HPP
#define GWRITING_HPP

#include "GTexture.hpp"
#include "DeleterTemplates.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

namespace hsdl
{

class GWriting : public GTexture
{
  public:
    GWriting();
    ~GWriting() = default;

// "writing" is written onto a GTexture::texture and returns false if failed
    bool CreateWrittenWord(SDL_Renderer* renderer, const std::string& writing);


// Loads font and assigns font_ptr. Emits error if font_ptr is nullptr.
// Kerning is set to false

    bool Load_font(const std::string& font_name, int font_size);

// Sets textColour
    void Set_WritingColour(Uint8 red, Uint8 green, Uint8 blue);


// Checks to see if "writing" will fit in pixel "width". If it does it creates
// CreateWrittenWord(renderer, writing). If not it look for the "token" and spilt writing there, checking
// against width and returning the unused string in "partial_string"

    std::string FitStringInPxl_Width(SDL_Renderer* renderer, const std::string & writing, uint16_t width,
                                const std::string & token = " ");

  private:
    TTF_Font* font_ptr;
    SDL_Color textColour;

};

}   // ENDOF namespace hsdl
#endif