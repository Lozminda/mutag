#ifndef SMALLTEXTURE_HPP
#define SMALLTEXTURE_HPP
// #define NDEBUG
#include "GTexture.hpp"
#include "ImageParameters.hpp"

#include <string>

class SmallTexture: public hsdl::GTexture
{
  public:
    SmallTexture();

    ImageParams imagePs;
    std::string filename;
    // bool IPs_alreadySet;

    bool flipped;

    void ResetFileNFlips();

    void SplitFilenameIntoPosMembers(uint64_t & lvl,
                                      ltcd::LetterCoords & fPx,
                                      ltcd::LetterCoords & fPy);

    void Calc_MPFs(uint64_t level);

    bool Load_OneColourInfo(uint64_t& oneColourOrItrtn);

  private:
    mpf_t MPF_LEFT;
    mpf_t MPF_TOP;
};

#endif