#ifndef DELETERTEMPLATES_HPP
#define DELETERTEMPLATES_HPP

template <typename Object, void (*DeleterFun)(Object*)>
struct Deleter;

#include "DeleterTemplates.tpp"

#endif