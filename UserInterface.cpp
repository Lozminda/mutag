#include "Constants.hpp"
#include "FileHandler.hpp"
#include "UserInterface.hpp"
#include "Position.hpp"
#include "ThreadPool.hpp"
#include "ImageParameters.hpp"

// #include <type_traits>
#include <string>
#include <sstream>
#include <future>
#include <cstdint>

#include <SDL2/SDL.h>

// ************************** TO DO *****************************

// ALSO NEED TO FIND A WAY TO PASS ImageParamsMPFs TO Display() AND PERHAPS SDLEnvironment
// NEEDS A RETHINK

// *********************  END OF TO DO  *************************

// Small box on top left is 272,  which is 136 *2 . 680/5 is 136 for route
// info. Should frames be a part of window enviroment. Or should the be made up of

// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
// position.fileSystem_y = mandelbrot_resourcer.MIN; should be position.fileSystem_y = position.fileSystem.MIN;
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

bool UserInterface::left_MseButtonPressed = false;
// constexpr uint16_t SCREEN_CENTRE = static_cast<uint16_t>(Graphics::TILE_SIZE_PXLS / 2);

UserInterface::UserInterface(bool automatic)
            :info_options()                                     // vvvvvv Not particularly attractive I know vvvvvv
            ,graphicsWindow(966, 688, Graphics::TILE_SIZE_PXLS, static_cast<Information_options>(0))
            ,mandelbrot_resourcer(graphicsWindow.RendererPtr.get(), automatic)
            ,Automatic{automatic}
            // <aybe automatc should be passed to M_R ?
{}


// ************************** TO DO *****************************
//     NOT SO SURE ABOUT THIS NOW !!                       vvvvvvvvvvvvvvvvv
//if(!keyBoardHandler(sdl_event, position, currentIPs.UserDefined, IMAGESTATECHANGED)) running = false;
//                                                         ^^^^^^^^^^^^^^^^^
// *********************  END OF TO DO  *************************

// the directions (left, right, down up and zooms) need Position and ImageParams UserDefined passing to
// them because if a boundry is changed then mandelbrot_resourcer.Texture need re-rendering and those
// params are neccessary. More effient to do if inside left right etc, than passing a signal out
// and then calling Texturise just before Display();

void UserInterface::MainLoop()
{
  Position position;
  ImageParams currentIPs;
  assign_values(position, currentIPs);

  if (Automatic) mandelbrot_resourcer.CreateListOfFilesToCalcd();

  graphicsWindow.Display(mandelbrot_resourcer.CompleteTexture, position,
                          info_options, currentIPs);

// Automatic Comments for automatic process. gives list of files to calc
// mandelbrot_resourcer::OrganiseTileData(position)
  if (!Automatic)
  {
    mandelbrot_resourcer.OrganiseTileData(position);
  }

  mandelbrot_resourcer.Texturise(graphicsWindow.RendererPtr.get(), position, currentIPs.UserDefined);

  bool running = true;
  bool once = false;

  SDL_Event sdl_event;

  while (running)                                 //Main Loop
  {

    if (!Automatic)
    {
      mandelbrot_resourcer.DoNewImagesNeed_Rendering(graphicsWindow.RendererPtr.get(),
                                                      position, currentIPs.UserDefined);
    }
    else
    {
      mandelbrot_resourcer.UpDateImagesToBeCalculated(position);
    }
    // Checkand_ManageEvents();
    if (sdl_event.type == SDL_QUIT) running = false;
    SDL_FlushEvents(SDL_USEREVENT,SDL_LASTEVENT);
    while (SDL_PollEvent(&sdl_event)!=0)
    {
      mouseHandler(sdl_event, position, currentIPs.UserDefined);
      if(!keyBoardHandler(sdl_event, position, currentIPs.UserDefined)) running = false;
    }
    mouseButtonHandler(UserInterface::left_MseButtonPressed, position, currentIPs.UserDefined);

    graphicsWindow.Display(mandelbrot_resourcer.CompleteTexture, position,
                            info_options, currentIPs);

    mandelbrot_resourcer.Process_Uncalculated_Images(graphicsWindow.RendererPtr.get());

    // SDL_Delay();
  }
  SaveType_inFile(std::string(std::string(Default::PATH) + std::string(Default::MAIN_LOG_FILENAME)),
                  position, FileHandler::NEW_FILE);
  SaveType_inFile(std::string(std::string(Default::PATH) + std::string(Default::MAIN_LOG_FILENAME)),
                  currentIPs);
  return;
}

void UserInterface::mouseButtonHandler(bool button_pressed, Position & position, UserDefined_s& user_params)
{
  if (button_pressed)
  {
    position.mouseX = mouseStateX - X_ADJUSTMENT;                  // position of mandelbrot viewport +/- frame
    position.mouseY = mouseStateY - Y_ADJUSTMENT;
    if (position.mouseX < SCREEN_CENTRE)
    {
      position.change =  (SCREEN_CENTRE - position.mouseX) / 7/*movement_rate*/;
      left(position, user_params);
    }
    if (position.mouseX > SCREEN_CENTRE)
    {
      position.change = (position.mouseX - SCREEN_CENTRE) / 7/*movement_rate*/;
      right(position, user_params);
    }
    if (position.mouseY < SCREEN_CENTRE)
    {
      position.change = (SCREEN_CENTRE - position.mouseY) / 7/*movement_rate*/;
      up(position, user_params);
    }
    if (position.mouseY > SCREEN_CENTRE)
    {
      position.change = (position.mouseY - SCREEN_CENTRE) / 7/*movement_rate*/;
      down(position, user_params);
    }
  }
  return;
}

// ----------------------------------- PRIVATE METHODS -----------------------------------

// As with all horizontal directions, there's position.mouse within the window, (position.mouseX&Y), windowPositions relative to
// largeTexture and then where all that sits in the system of files. left() right() up() & down() take
// care of all hence the logic getting a bit of a pain...
// position.change is either the speed of the cross hairs on the window or the speed at which the window
// moves across the texture

bool UserInterface::keyBoardHandler(SDL_Event& sdl_event,  Position& position, UserDefined_s& user_params)
{
  if (sdl_event.type == SDL_KEYDOWN)
  {
    switch (sdl_event.key.keysym.sym)
    {
      case SDLK_MINUS:
        zoomOut(position, user_params);
      break;
      case SDLK_EQUALS:
        zoomIn(position, user_params);
      break;
      case SDLK_LEFT:
        left(position, user_params);
      break;
      case SDLK_RIGHT:
        right(position, user_params);
      break;
      case SDLK_UP:
        up(position, user_params);
      break;
      case SDLK_DOWN:
        down(position, user_params);
      break;
      case SDLK_ESCAPE:
        return false;
      break;
      case SDLK_q:
        if (position.change < 340 ) position.change += 5;
        std::cout<<"position.change: "<<position.change<<"\n";
      break;
      case SDLK_a:
        if (position.change > 5 ) position.change -= 5;
        std::cout<<"position.change: "<<position.change<<"\n";
      break;
      case SDLK_d:
        position.change = 68;
        std::cout<<"position.change: "<<position.change<<"\n";
      break;
      case SDLK_i:
        toggle_info();
      break;
      case SDLK_SPACE:
        toggle_imagePercentage(user_params);
      break;
      case SDLK_m:
        if (user_params.maxIterations < Default::MAX_ITERATIONS)             // 2^31 -1
        {
          user_params.maxIterations *= 2;
          ImageParams::maxIterationsHaveChanged = true;
          ImageParams::ImageParamChanged = true;
        }
      break;
      case SDLK_n:
        if (user_params.maxIterations > Default::MIN_ITERATIONS)             // 2^31 -1
        {
          user_params.maxIterations -= Default::MIN_ITERATIONS;
          ImageParams::maxIterationsHaveChanged = true;
          ImageParams::ImageParamChanged = true;
        }
      break;
      case SDLK_k:
        user_params.maxIterations = Default::BASELINE_ITERATIONS;
        ImageParams::maxIterationsHaveChanged = true;
        ImageParams::ImageParamChanged = true;
      break;

    }
  }
  return true;
}

void UserInterface::mouseHandler(SDL_Event& sdl_event,  Position& position, UserDefined_s& user_params)
{
  int positionChangeX = 0;
  int positionChangeY = 0;
  static int mousewheelzoom = 0;
  static bool centreMouseFirst = true;
  if ((sdl_event.type == SDL_MOUSEMOTION) || (sdl_event.type == SDL_MOUSEBUTTONDOWN)
        || (sdl_event.type == SDL_MOUSEBUTTONUP) || (sdl_event.type == SDL_MOUSEWHEEL))
  {
    SDL_GetMouseState(&mouseStateX, &mouseStateY);
    if ((sdl_event.type == SDL_MOUSEBUTTONDOWN) && (sdl_event.button.button == SDL_BUTTON_RIGHT))
    {

      position.mouseX = mouseStateX - X_ADJUSTMENT;               // position of mandelbrot viewport +/- frame
      position.mouseY = mouseStateY - Y_ADJUSTMENT;
      positionChangeX = SCREEN_CENTRE - position.mouseX;
      positionChangeY = SCREEN_CENTRE - position.mouseY;
      if (positionChangeX < 0)
      {
        position.change = - positionChangeX;
        right(position, user_params);
      }
      else
      {
        position.change = positionChangeX;
        left(position, user_params);
      }
      if (positionChangeY < 0)
      {
        position.change = - positionChangeY;
        down(position, user_params);
      }
      else
      {
        position.change = positionChangeY;
        up(position, user_params);
      }
      SDL_WarpMouseInWindow(nullptr, (SCREEN_CENTRE + X_ADJUSTMENT),
                            (SCREEN_CENTRE + Y_ADJUSTMENT));
    }
    if ((sdl_event.type == SDL_MOUSEBUTTONDOWN) && (sdl_event.button.button == SDL_BUTTON_LEFT))
    {
      UserInterface::left_MseButtonPressed = true;
    }
    else if ((sdl_event.type == SDL_MOUSEBUTTONUP) && (sdl_event.button.button == SDL_BUTTON_LEFT))
    {
      UserInterface::left_MseButtonPressed = false;
      // std::cout<<" BUTTON UP UserInterface::left_MseButtonPressed: "<<UserInterface::left_MseButtonPressed<<"\n";
    }
    else if (sdl_event.type == SDL_MOUSEMOTION)
    {
      position.mouseX = mouseStateX - X_ADJUSTMENT;                  // position of mandelbrot viewport +/- frame
      position.mouseY = mouseStateY - Y_ADJUSTMENT;                                // thickness of frame
    }
    else if(sdl_event.type == SDL_MOUSEWHEEL)
    {
      if(sdl_event.wheel.y > 0) ++mousewheelzoom;
      else if(sdl_event.wheel.y < 0) --mousewheelzoom;
      SDL_FlushEvents(SDL_USEREVENT,SDL_LASTEVENT);
    }
    if (mousewheelzoom > 2)
    {
      zoomIn(position, user_params);
      mousewheelzoom = 0;
    }
    if (mousewheelzoom < -2)
    {
      zoomOut(position, user_params);
      mousewheelzoom = 0;
    }

  }   // ENDOF else if SDL_MOUSEMOTION) || SDL_MOUSEBUTTONDOWN [53]
  else
  {
    if (centreMouseFirst)
    {
      SDL_WarpMouseInWindow(nullptr, (SCREEN_CENTRE + X_ADJUSTMENT),
        (SCREEN_CENTRE + Y_ADJUSTMENT));
      centreMouseFirst = false;
    }
  }
  return;
}

void UserInterface::left(Position& position, UserDefined_s& user_params)
{
  if (position.mouseX > SCREEN_CENTRE) position.mouseX -= position.change;
  else
  {
    if (position.fileSystem_x == Position::FILE_SYS_MIN)
    {
      if ((position.viewTopLeft_X <= 0) && (position.mouseX > 0)) position.mouseX -= position.change;
      if (position.viewTopLeft_X >= position.change) position.viewTopLeft_X -= position.change;
    }
    else                                                // if (fileSystem_x != Position::FILE_SYS_MIN)
    {
      if (position.viewTopLeft_X <= Graphics::TILE_SIZE_PXLS )
      {
        --position.fileSystem_x;

        mandelbrot_resourcer.OrganiseTileData(position);
        mandelbrot_resourcer.Texturise(graphicsWindow.RendererPtr.get(), position, user_params);

        position.viewTopLeft_X = mandelbrot_resourcer.WidthPxls - Graphics::TILE_SIZE_PXLS - position.change;
      }
      else if (position.mouseX > SCREEN_CENTRE) position.mouseX -= position.change;
      else position.viewTopLeft_X -= position.change;
    }
  }
  if (position.viewTopLeft_X < 0) position.viewTopLeft_X = 0;
  return;
}

void UserInterface::right(Position& position, UserDefined_s& user_params)
{
  if (position.fileSystem_x != mandelbrot_resourcer.xMAX)
  {
    if (position.mouseX < SCREEN_CENTRE) position.mouseX += position.change;
    else if (position.viewTopLeft_X < (mandelbrot_resourcer.WidthPxls - Graphics::TILE_SIZE_PXLS - position.change))
      position.viewTopLeft_X += position.change;
    else
    {
      ++position.fileSystem_x;

        mandelbrot_resourcer.OrganiseTileData(position);
        mandelbrot_resourcer.Texturise(graphicsWindow.RendererPtr.get(), position, user_params);
      if (position.fileSystem_x != mandelbrot_resourcer.xMAX) position.viewTopLeft_X = Graphics::TILE_SIZE_PXLS + position.change;
      else position.viewTopLeft_X = Graphics::TILE_SIZE_PXLS;
    }
  }
  else if (position.mouseX < Graphics::TILE_SIZE_PXLS) position.mouseX += position.change;
  return;
}

void UserInterface::up(Position& position, UserDefined_s& user_params)
{
  if (position.small_TextureBelowXaxis && (position.fileSystem_y != mandelbrot_resourcer.yMAX) ) REALBelowXaxis = true;
  if (!position.small_TextureBelowXaxis && (position.fileSystem_y != mandelbrot_resourcer.yMAX) ) REALBelowXaxis = false;


// position.small_TextureBelowXaxis serves the tiling of textures, a more accurate one is needed for zoomIn and Out, hence:-

  if ( (position.viewTopLeft_Y - (mandelbrot_resourcer.HeightPxls - (2 * Graphics::TILE_SIZE_PXLS)) - position.change < SCREEN_CENTRE)

    && (position.mouseY == SCREEN_CENTRE) && !position.small_TextureBelowXaxis && (position.fileSystem_y == mandelbrot_resourcer.yMAX) )
      REALBelowXaxis = false;

  if (position.mouseY > SCREEN_CENTRE) position.mouseY -= position.change;
  else if (position.fileLevel > 2)
  {
    if (position.viewTopLeft_Y <= mandelbrot_resourcer.HeightPxls - (2 * Graphics::TILE_SIZE_PXLS))
    {
      if (position.small_TextureBelowXaxis)
      {
        if (position.fileSystem_y == mandelbrot_resourcer.yMAX)
        {
          position.small_TextureBelowXaxis = false;

        mandelbrot_resourcer.OrganiseTileData(position);
        mandelbrot_resourcer.Texturise(graphicsWindow.RendererPtr.get(), position, user_params);
          position.viewTopLeft_Y = mandelbrot_resourcer.HeightPxls - Graphics::TILE_SIZE_PXLS - position.change;
        }
        else
        {
// I thought I could move some of these ++filepos_y s out of the various scopes to simply the logic
// It should be possible, but then it didn't work, hence the ungainliness...
          if (position.fileSystem_y == Position::FILE_SYS_MIN)
          {
            ++position.fileSystem_y;
            if (position.fileSystem_y == mandelbrot_resourcer.yMAX) position.small_TextureBelowXaxis = false;
            else ++position.fileSystem_y;

        mandelbrot_resourcer.OrganiseTileData(position);
        mandelbrot_resourcer.Texturise(graphicsWindow.RendererPtr.get(), position, user_params);
            position.viewTopLeft_Y = mandelbrot_resourcer.HeightPxls - Graphics::TILE_SIZE_PXLS - position.change;
          }
          else
          {
            ++position.fileSystem_y;

            mandelbrot_resourcer.OrganiseTileData(position);
        mandelbrot_resourcer.Texturise(graphicsWindow.RendererPtr.get(), position, user_params);
            position.viewTopLeft_Y = mandelbrot_resourcer.HeightPxls - Graphics::TILE_SIZE_PXLS - position.change;
          }
        }
      }
      else if (position.fileSystem_y != Position::FILE_SYS_MIN)
      {
        --position.fileSystem_y;

        mandelbrot_resourcer.OrganiseTileData(position);
        mandelbrot_resourcer.Texturise(graphicsWindow.RendererPtr.get(), position, user_params);
        if (position.fileSystem_y != Position::FILE_SYS_MIN) position.viewTopLeft_Y = Graphics::TILE_SIZE_PXLS * 2 - position.change;
      }
    }   // ENDOF  py <= lP.h - 2 * SS
    else position.viewTopLeft_Y -= position.change;
  }
  else /*if (position.fileLevel == 2)*/
  {
    if (position.viewTopLeft_Y < SCREEN_CENTRE) position.small_TextureBelowXaxis = false;
    if (position.viewTopLeft_Y >= 0) position.viewTopLeft_Y -= position.change;
  }
  if  ((position.mouseY >= position.change) && (position.fileSystem_y == Position::FILE_SYS_MIN)
    && !position.small_TextureBelowXaxis && (position.viewTopLeft_Y <= 0)) position.mouseY -= position.change;
  if (position.viewTopLeft_Y < 0) position.viewTopLeft_Y = 0;

  // printOut();
  return;
}

void UserInterface::down(Position& position, UserDefined_s& user_params)
{
  if (position.small_TextureBelowXaxis && (position.fileSystem_y != mandelbrot_resourcer.yMAX) ) REALBelowXaxis = true;
  if (!position.small_TextureBelowXaxis && (position.fileSystem_y != mandelbrot_resourcer.yMAX) ) REALBelowXaxis = false;

// position.small_TextureBelowXaxis serves the tiling of textures, a more accurate one is needed for zoomIn and Out, hence:-
  if ( (position.viewTopLeft_Y - (mandelbrot_resourcer.HeightPxls - (2 * Graphics::TILE_SIZE_PXLS)) + position.change > SCREEN_CENTRE) && (position.mouseY == SCREEN_CENTRE)
    && !position.small_TextureBelowXaxis && (position.fileSystem_y == mandelbrot_resourcer.yMAX) ) REALBelowXaxis = true;

  if (position.mouseY < SCREEN_CENTRE) position.mouseY += position.change;
  else
  {
    if (position.viewTopLeft_Y >= mandelbrot_resourcer.HeightPxls - Graphics::TILE_SIZE_PXLS)
    {
      if (position.small_TextureBelowXaxis)
      {
        if (position.fileSystem_y != Position::FILE_SYS_MIN)
        {
          --position.fileSystem_y;
          if (position.fileSystem_y != Position::FILE_SYS_MIN) position.viewTopLeft_Y = Graphics::TILE_SIZE_PXLS + position.change;
          else position.viewTopLeft_Y = Graphics::TILE_SIZE_PXLS;

          mandelbrot_resourcer.OrganiseTileData(position);
          mandelbrot_resourcer.Texturise(graphicsWindow.RendererPtr.get(), position, user_params);

        }
        else
        {
          if (position.mouseY < Graphics::TILE_SIZE_PXLS) position.mouseY += position.change;
        }
      }
      else //(above position.fileSystem_x axis)
      {
        if ((position.fileSystem_y == mandelbrot_resourcer.yMAX) && (position.fileLevel > 1))
        {
          position.small_TextureBelowXaxis = true;
          if (position.fileSystem_y != Position::FILE_SYS_MIN) position.viewTopLeft_Y = Graphics::TILE_SIZE_PXLS + position.change;
          else position.viewTopLeft_Y = Graphics::TILE_SIZE_PXLS;
        }
        else if (position.fileLevel > 1)
        {
          ++position.fileSystem_y;
          position.viewTopLeft_Y = Graphics::TILE_SIZE_PXLS + position.change;
        }
        else if (position.mouseY < Graphics::TILE_SIZE_PXLS) position.mouseY += position.change;

        mandelbrot_resourcer.OrganiseTileData(position);
        mandelbrot_resourcer.Texturise(graphicsWindow.RendererPtr.get(), position, user_params);
      }
    }
    else position.viewTopLeft_Y += position.change;
  }
  // printOut();

  return;
}

// get real positions (pos of position.mouse etc) halve (halve letter coords too)
// then work out where top left corner of window should be and voila zoom out
// levels 1&2 are a bit of a pain hence the exceptional if !

void UserInterface::zoomOut(Position& position, UserDefined_s& user_params)                                          // Four pics to one pic
{
  if (position.fileLevel >= 2)
  {
    if (position.fileLevel == 2)
    {
      posOnMandel_textureX = position.viewTopLeft_X + position.mouseX;
      posOnMandel_textureY = position.viewTopLeft_Y + position.mouseY;
      position.mouseY = 0;
      position.mouseX = 0;
      if (posOnMandel_textureX > Graphics::TILE_SIZE_PXLS) position.mouseX += SCREEN_CENTRE;
      if (posOnMandel_textureY > Graphics::TILE_SIZE_PXLS) position.mouseY += SCREEN_CENTRE;
      if (posOnMandel_textureX % Graphics::TILE_SIZE_PXLS != 0) posOnMandel_textureX = posOnMandel_textureX % Graphics::TILE_SIZE_PXLS;
      if (posOnMandel_textureY % Graphics::TILE_SIZE_PXLS != 0) posOnMandel_textureY = posOnMandel_textureY % Graphics::TILE_SIZE_PXLS;
      position.mouseY += (posOnMandel_textureY / 2);
      position.mouseX += (posOnMandel_textureX / 2);
      position.viewTopLeft_Y = 0;
      position.viewTopLeft_X = 0;
      position.fileSystem_y = Position::FILE_SYS_MIN;
      position.fileSystem_x = Position::FILE_SYS_MIN;
      position.small_TextureBelowXaxis = false;
      REALBelowXaxis = false;
      position.fileLevel = 1;
      mandelbrot_resourcer.xMAX.PowerTwo(position.fileLevel - 1);
      mandelbrot_resourcer.yMAX.PowerTwo(0);
      mandelbrot_resourcer.OrganiseTileData(position);
    }
    else
    {
      getPositionOnMandel_texture(position);
      posOnMandel_textureX /= 2;
      posOnMandel_textureY /= 2;
      if (position.fileSystem_x.divideBy2()) posOnMandel_textureX += SCREEN_CENTRE;
      if (position.fileSystem_y.divideBy2()) posOnMandel_textureY += SCREEN_CENTRE;
      --position.fileLevel;
      calcViewTopLeft_XY_fromPositionOnMandel_texture(position);
    }
  }
  mandelbrot_resourcer.Texturise(graphicsWindow.RendererPtr.get(), position, user_params);

  // printOut();
  return;
}

// Times by 2 instead of divide

void UserInterface::zoomIn(Position& position, UserDefined_s& user_params)
{
  // if (position.fileLevel < 8)
  {
    if (position.fileLevel == 1)
    {
      // std::cout<<"position.mouseX: "<<position.mouseX<<"\n";
      posOnMandel_textureX = position.mouseX;
      posOnMandel_textureY = position.mouseY;
      position.viewTopLeft_Y = 0;
      position.viewTopLeft_X = 0;
      posOnMandel_textureX = posOnMandel_textureX * 2;
      posOnMandel_textureY = posOnMandel_textureY * 2;

      position.fileLevel = 2;
      mandelbrot_resourcer.OrganiseTileData(position);

      position.viewTopLeft_X += posOnMandel_textureX - SCREEN_CENTRE;
      position.viewTopLeft_Y += posOnMandel_textureY - SCREEN_CENTRE;

      if (position.viewTopLeft_X < 0)
      {
        position.mouseX = posOnMandel_textureX;
        position.viewTopLeft_X = 0;
      }
      else if (position.viewTopLeft_X > Graphics::TILE_SIZE_PXLS)
      {
        position.mouseX = posOnMandel_textureX - Graphics::TILE_SIZE_PXLS;
        position.viewTopLeft_X = Graphics::TILE_SIZE_PXLS;
      }
      else position.mouseX = SCREEN_CENTRE;

      if(position.viewTopLeft_Y < 0)
      {
        position.mouseY = posOnMandel_textureY;
        position.viewTopLeft_Y = 0;
      }
      else if (position.viewTopLeft_Y > Graphics::TILE_SIZE_PXLS)
      {
        position.mouseY = posOnMandel_textureY - Graphics::TILE_SIZE_PXLS;
        position.viewTopLeft_Y = Graphics::TILE_SIZE_PXLS;
      }
      else position.mouseY = SCREEN_CENTRE;
    }
    else
    {
      getPositionOnMandel_texture(position);
      posOnMandel_textureY *= 2;
      posOnMandel_textureX *= 2;
      position.fileSystem_y.times2();
      position.fileSystem_x.times2();
      ++position.fileLevel;
      calcViewTopLeft_XY_fromPositionOnMandel_texture(position);
    }
    mandelbrot_resourcer.Texturise(graphicsWindow.RendererPtr.get(), position, user_params);
  }
  return;
}


// AT SOME POINT NEED TO GT RID OF THE TESTING FUNCTIONALITY
// To find out where the centre of the screen really is (or the position.mouse if it's not centred)
// hence posOnMandel_textureX&Y

void UserInterface::getPositionOnMandel_texture(Position& position)
{
  posOnMandel_textureX = position.viewTopLeft_X + position.mouseX;
  posOnMandel_textureY = position.viewTopLeft_Y + position.mouseY;

// ----------------------- Y ----------------------
  if (mandelbrot_resourcer.HeightPxls == 3 * Graphics::TILE_SIZE_PXLS) posOnMandel_textureY -= Graphics::TILE_SIZE_PXLS;
  if (position.fileLevel != 1)
  {
    if (REALBelowXaxis)
    {
      if (posOnMandel_textureY > Graphics::TILE_SIZE_PXLS)
      {
        if (!position.small_TextureBelowXaxis) position.small_TextureBelowXaxis = true;
        else
        {
          if (position.fileSystem_y != Position::FILE_SYS_MIN) --position.fileSystem_y;
        }
        posOnMandel_textureY -= Graphics::TILE_SIZE_PXLS;
      }
    }
    else //if above X axis
    {
      if (position.fileSystem_y == mandelbrot_resourcer.yMAX)
      {
        if (posOnMandel_textureY > Graphics::TILE_SIZE_PXLS)
        {
          REALBelowXaxis = true;
          position.small_TextureBelowXaxis = true;
          posOnMandel_textureY -= Graphics::TILE_SIZE_PXLS;                                  // should get picked up by line 439 =>
        }
      }
      else if (posOnMandel_textureY > Graphics::TILE_SIZE_PXLS)  //(fileposy== ANYTHING BUT MAX)
      {
        if (position.fileSystem_y != mandelbrot_resourcer.yMAX)
        {
         ++position.fileSystem_y;
          posOnMandel_textureY -= Graphics::TILE_SIZE_PXLS;
        }
      }
    }   // ENDOF  else if above X axis
  }   // ENDOF position.fileLevel > 1

// ----------------------- X ----------------------
  if (mandelbrot_resourcer.WidthPxls == 3 * Graphics::TILE_SIZE_PXLS) posOnMandel_textureX -= Graphics::TILE_SIZE_PXLS;
  if (position.fileLevel != 1)
  {
    if (posOnMandel_textureX > Graphics::TILE_SIZE_PXLS)
    {
      if (position.fileSystem_x != mandelbrot_resourcer.xMAX)
      {
        ++position.fileSystem_x;
        // posOnMandel_textureX -= Graphics::TILE_SIZE_PXLS;
      }
      posOnMandel_textureX -= Graphics::TILE_SIZE_PXLS;
    }
  }
// ------------------------------------------------
  if (REALBelowXaxis) posOnMandel_textureY = Graphics::TILE_SIZE_PXLS - posOnMandel_textureY;
  // printOut();
  return;
}


void UserInterface::calcViewTopLeft_XY_fromPositionOnMandel_texture(Position& position)
{

  mandelbrot_resourcer.xMAX.PowerTwo(position.fileLevel - 1);
  if (position.fileLevel == 1) mandelbrot_resourcer.yMAX.PowerTwo(0);
  else mandelbrot_resourcer.yMAX.PowerTwo(position.fileLevel - 2);

  // mandelbrot_resourcer.filesPerLevel_OK();                                           // THIS RETURNS TRUE OR FALSE NOT USED
  mandelbrot_resourcer.OrganiseTileData(position);

// ------------------------- X -------------------------

  position.viewTopLeft_X = posOnMandel_textureX - SCREEN_CENTRE;
  if (position.viewTopLeft_X > Graphics::TILE_SIZE_PXLS)
  {
    ++position.fileSystem_x;                                                // In theory if all calc right this can never go
    position.viewTopLeft_X -= Graphics::TILE_SIZE_PXLS;                                  // over mandelbrot_resourcer.xMAX
    mandelbrot_resourcer.OrganiseTileData(position);


  }
  if (position.viewTopLeft_X < 0) // should this be less than SS if lp.w=3*SS ?
  {
    if (position.fileSystem_x != Position::FILE_SYS_MIN)
    {
      --position.fileSystem_x;
      mandelbrot_resourcer.OrganiseTileData(position);

      position.viewTopLeft_X = Graphics::TILE_SIZE_PXLS + position.viewTopLeft_X;
    }
    else
    {
      position.mouseX = posOnMandel_textureX;
      position.viewTopLeft_X = 0;
    }
  }
  if (position.fileSystem_x == mandelbrot_resourcer.xMAX)
  {
    if (mandelbrot_resourcer.WidthPxls == 2 * Graphics::TILE_SIZE_PXLS)  position.viewTopLeft_X += Graphics::TILE_SIZE_PXLS;
    if (position.viewTopLeft_X > Graphics::TILE_SIZE_PXLS)
    {
      position.mouseX = posOnMandel_textureX;
      if (position.mouseX > Graphics::TILE_SIZE_PXLS) position.mouseX -= Graphics::TILE_SIZE_PXLS;
      position.viewTopLeft_X = Graphics::TILE_SIZE_PXLS;
    }
  }
  if (mandelbrot_resourcer.WidthPxls == 3 * Graphics::TILE_SIZE_PXLS) position.viewTopLeft_X += Graphics::TILE_SIZE_PXLS;

  // ----------------------- Y -------------------------

  if (!REALBelowXaxis)
  {
    position.viewTopLeft_Y = posOnMandel_textureY - SCREEN_CENTRE;
    if (position.viewTopLeft_Y > Graphics::TILE_SIZE_PXLS)
    {
      if (position.fileSystem_y != mandelbrot_resourcer.yMAX) ++position.fileSystem_y;
      position.viewTopLeft_Y -= Graphics::TILE_SIZE_PXLS;
      mandelbrot_resourcer.OrganiseTileData(position);


    }
    if (position.viewTopLeft_Y < 0)
    {
      if (position.fileSystem_y != Position::FILE_SYS_MIN)
      {
        --position.fileSystem_y;
        mandelbrot_resourcer.OrganiseTileData(position);


        position.viewTopLeft_Y = Graphics::TILE_SIZE_PXLS + position.viewTopLeft_Y;
      }
      else
      {
        position.mouseY = posOnMandel_textureY;
        position.viewTopLeft_Y = 0;
      }
    }
  }
  else // if below x axis
  {
    if (posOnMandel_textureY > Graphics::TILE_SIZE_PXLS)
    {
      ++position.fileSystem_y;
      mandelbrot_resourcer.OrganiseTileData(position);


      posOnMandel_textureY -= Graphics::TILE_SIZE_PXLS;
    }
    assert( ((posOnMandel_textureY <= Graphics::TILE_SIZE_PXLS) && " here at calcViewTopLeft_XYfromReals\n") );
    posOnMandel_textureY = Graphics::TILE_SIZE_PXLS - posOnMandel_textureY;
    position.viewTopLeft_Y = posOnMandel_textureY - SCREEN_CENTRE;
    if (position.viewTopLeft_Y < 0)
    {
// this little section sorts out below X axis but close to it 15.08 13.13
      if (position.fileSystem_y == mandelbrot_resourcer.yMAX)
      {
        position.small_TextureBelowXaxis = false;
        REALBelowXaxis = false;
      }
      else ++position.fileSystem_y;
// -------------------------------------------------------
      mandelbrot_resourcer.OrganiseTileData(position);


      position.viewTopLeft_Y = Graphics::TILE_SIZE_PXLS + position.viewTopLeft_Y;
    }
    if (mandelbrot_resourcer.HeightPxls == 2 * Graphics::TILE_SIZE_PXLS)
    {
      std::cout<<"\n$%$%$ZZZZ\n";                                // JUST FOR TESTING ATM
      position.viewTopLeft_Y += Graphics::TILE_SIZE_PXLS;
    }
    if ((position.viewTopLeft_Y > Graphics::TILE_SIZE_PXLS) && (position.fileSystem_y == Position::FILE_SYS_MIN))
    {
      position.mouseY = posOnMandel_textureY;
      if (position.mouseY > Graphics::TILE_SIZE_PXLS) position.mouseY -= Graphics::TILE_SIZE_PXLS;
      position.viewTopLeft_Y = Graphics::TILE_SIZE_PXLS;
    }
  }
  if (mandelbrot_resourcer.HeightPxls == 3 * Graphics::TILE_SIZE_PXLS) position.viewTopLeft_Y += Graphics::TILE_SIZE_PXLS;
  return;
}

void UserInterface::assign_values(Position& pos, ImageParams& imagePs)
{
  if (!Automatic)
  {
    const std::string wholeLogFilename = std::string(Default::PATH) +
    std::string(Default::MAIN_LOG_FILENAME);
    if ( DoesFileExist(wholeLogFilename) )
    {
      auto vs = StringsFromFile(wholeLogFilename);
      pos.Initialise(vs[0], vs[1], vs[2], vs[3], vs[4], vs[5], vs[6], vs[7], vs[8]);
      imagePs.Initialise(vs[9], vs[10], vs[11], vs[12], vs[13], vs[14], vs[15], vs[16], vs[17]);
    }
    else std::cerr<<"Main Log File Doesn't Exist, Using Defaults\n";
  }
  else
// in case defaults aren't set to values neccessary for automatic process
  {
    pos.fileLevel = 1;
    imagePs.UserDefined.resolution_X = 1;
    imagePs.UserDefined.resolution_Y = 1;
    imagePs.UserDefined.imageState = 'N';
    imagePs.UserDefined.maxIterations = Default::START_ITERATIONS;
  }

  REALBelowXaxis = pos.small_TextureBelowXaxis;

  return;
}

void UserInterface::toggle_info()
{
  if (info_options == keyBindings)
  {
    info_options = mouse;
    return;
  }
  if (info_options == mouse)
  {
    info_options = co_ordinates;
    return;
  }
  if (info_options == co_ordinates)
  {
    info_options = instructions;
    return;
  }
  if (info_options == instructions)
  {
    info_options = keyBindings;
    return;
  }
}

void UserInterface::toggle_imagePercentage(UserDefined_s& user_params)
{
  ImageParams::ImageParamChanged = true;
  user_params.toggle();
  return;
}