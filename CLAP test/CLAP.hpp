#ifndef CLAP
#define CLAP

#include <string>
#include <vector>
#include <map>
#include <exception>


class CLAProcessor
{
  public:

    CLAProcessor() = default;

    CLAProcessor(int, char const **/*, const std::string &, const std::string &*/);

    ~CLAProcessor() = default;

    CLAProcessor(CLAProcessor const& other) = default;
    CLAProcessor& operator=(CLAProcessor const& rhs) = default;

    friend std::ostream & operator << (std::ostream&, const CLAProcessor&);

    std::string Path;
    std::string Logfilename;
    uint64_t NumOfIterations;
    uint64_t Level;
    bool helpPageOutputted, StopBetweenImages;
    bool input_error;

  private:
    static const std::string help_instructions;
    static const std::map<std::string,int> options;
    static const uint64_t DEFAULT_LEVEL = 1;
    static const uint64_t DEFAULT_ITERATION = 250;
    std::vector<std::string> argVec;
    bool organiseComLineArgs();
    bool is_digits(const std::string &);
};

#endif