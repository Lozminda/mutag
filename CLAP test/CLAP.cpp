#include "CLAP.hpp"
#include <string>
#include <map>
#include <iostream>

const std::map<std::string,int>
      CLAProcessor::options {
                              {"-i",1},
                              {"-l",2},
                              {"-P",3},
                              {"-LF",4},
                              {"-h",5},
                              {"-H",6},
                              {"-S",7}
                            };

const std::string CLAProcessor::help_instructions =
  "\n                HELP for Mandelbrot Generator v 3.543 by LozCorp\n"
  "\n  Flags can be separated by a space or not. If there is a space then the next argument will be assigned"
  "\n  to that flag eg '-i500' and '-i' '500' are the same thing and will assign 500 to iterations...\n"
  "\n  -i  Max number of iterations per pixel calculation, default is 250. "
  "\n  -l  Level start, if no level is specified MGentor will start from level 2."
  "\n  -P  Path of saved images, don't forget the last / !"
  "\n  -LF Logfilename."
  "\n  -S  Stops between each image and asks a question or two"
  "\n  -h prints this information."
  "\n  -H prints this information.\n";



CLAProcessor::CLAProcessor(int numComLineArgs, char const **comLineArgStr/*,
                            const std::string &path, const std::string &logfn*/)
              :/*Path(path)
              ,Logfilename(logfn)
              ,*/NumOfIterations(DEFAULT_ITERATION)
              ,Level(DEFAULT_LEVEL)
              ,helpPageOutputted(false)
              ,StopBetweenImages(false)
              ,input_error(false)
{
  bool assignment_Finished = false;
  if (numComLineArgs == 1)
  {
    assignment_Finished = true;
  }
  else if (numComLineArgs > 1)
  {
    for (size_t it = 0; it < numComLineArgs; ++it)
    {
      std::string tempStr(comLineArgStr[it]);
      argVec.push_back(tempStr);
    }
  }
  if (!assignment_Finished)
  {
    organiseComLineArgs();
  }
}

bool CLAProcessor::is_digits(const std::string &str) {
    return str.find_first_not_of("0123456789") == std::string::npos;
}

bool CLAProcessor::organiseComLineArgs()
{

// this does if option is separate from number but also need to do
// if option is a part of string..
  bool noSpaceTwixt_FlagNArgument = false;
  bool noHelpFlagPassed = true;

  for (auto it_argVec = argVec.begin() + 1;
        it_argVec != argVec.end(); ++it_argVec)
  {
    for (auto itOptions = options.begin();
        itOptions != options.end(); ++itOptions)
    {

// find will find "-l" in "-l" as well as "-l500" as gotta make sure they're different lengths
// hence the second half of the logic statement (&& onwards)

      noSpaceTwixt_FlagNArgument = it_argVec->find(itOptions->first) != std::string::npos;
      noSpaceTwixt_FlagNArgument = noSpaceTwixt_FlagNArgument && (it_argVec->length() != (itOptions->first).length());

      if ((itOptions->first == *it_argVec) || noSpaceTwixt_FlagNArgument)
      {
        int selected_option = itOptions->second;
        switch(selected_option)
        {
          case 1 ... 2:
          {

// ********************************************************************************************
// THERE'S AN ERROR HERE, IF COM LINE ARGS ARE BOTH -I AND -L ONE WILL BE ASSIGNED AND NOT BOTH.
            // I don't think thsi is an error
// ********************************************************************************************

            uint64_t value = 0;
            if (noSpaceTwixt_FlagNArgument)
            {
              std::string argStr = *it_argVec;
              argStr.erase(0,(itOptions->first).length());
              if (!is_digits(argStr))
                throw std::invalid_argument("level or iteration argument contains non numeric characters \n");
              else value = stoi(argStr);
            }
            else
            {
              if (!is_digits(*(it_argVec + 1)))
                throw std::invalid_argument("level or iteration argument contains non numeric characters \n");
              else value = stoi(*(it_argVec + 1));
            }

            selected_option == 1 ? NumOfIterations = value : Level = value;

          }
          break;
// Level and NumOfIterations sorted but not the Path and Logfilename, currently set to defaults
// case 3 and 4 not written yet. Error handling seems to work so far...
          case 3 ... 4:
          {
            std::string valueStr;
            if (noSpaceTwixt_FlagNArgument)
            {
              std::string argStr = *it_argVec;
              argStr.erase(0,(itOptions->first).length());
              valueStr = argStr;
            }
            else
            {
              valueStr = *(it_argVec + 1);
            }
            selected_option == 3 ? Path = valueStr : Logfilename = valueStr;

          }
          break;
          case 5 ... 6:
          {
            std::cout<<help_instructions;
            helpPageOutputted = true;
          }
          case 7:
          {
            StopBetweenImages = true;
          }
          break;
          std::cout<<"Press a key\n";
          std::cin.ignore();
        }
      }
    }   // ENDOF for (itOptions)
  }   // ENDOF for (argVec)
  std::cout<<"Press a key\n";
  std::cin.ignore();
  std::cout<<"NumOfIterations: "<<NumOfIterations<<"\n";
  std::cout<<"Level: "<<Level<<"\n";
  std::cout<<"Path: "<<Path<<"\n";
  std::cout<<"Logfilename: "<<Logfilename<<"\n";

  return noHelpFlagPassed;
}

std::ostream & operator << (std::ostream &os, const CLAProcessor &var)
{
  std::vector<char> numChr;

  for(const auto &it  : numChr)
  {
    os<<it;
  }
  return os;
}
