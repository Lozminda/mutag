# -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self  -Wmissing-declarations -Wmissing-include-dirs -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-overflow=5 -Wswitch-default -Wundef -Werror -Wno-unused
# This variable might be 'overwritten' by a command line arg

# vvvvv important vvvvvv
# without VPATH the compiler's a moron
VPATH = FileHandling:Graphics:Ancillaries
# These are for string substitutions
DIR1 = FileHandling/
DIR2 = Graphics/
DIR3 = Ancillaries/
# ^^^^^ important ^^^^^^

CC = clang++-3.8
GMP = false
THREAD = false
PED = false
SDL2FLAGS = -lSDL2 -lSDL2_image -lSDL2_ttf
GMPFLAGS =  -lgmp -lgmpxx
THREADFLAGS = -pthread
LDTHEADFLAGS = -lpthread

PEDFLAGS = -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy\
-Wdisabled-optimization -Wformat=2 -Winit-self  -Wmissing-declarations\
-Wmissing-include-dirs -Wold-style-cast -Woverloaded-virtual -Wredundant-decls\
-Wshadow -Wsign-conversion -Wsign-promo -Wstrict-overflow=5 -Wswitch-default -Wundef\
-Werror -Wunused -Wextra  -Wunused-but-set-parameter -fdiagnostics-show-option

DEBUGFLAG =
SANITIZEFLAGS =  -fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls
LDLIBSXTRA = -lstdc++fs

# LDFLAGs for -L rather than -l LDFLAGS = ...
# lc++ for libstd clang gdb symbols
LDLIBS = -lc++ -lm -lgcc_s -lgcc -lc
# LDLIBS = -lm -lgcc_s -lgcc -lc

# Sorting the different flags out for different compilers and modes
# stdlib=libc++ if for gdb to "decode" std::string
ifeq ($(CC),g++-7)
  CXXFLAGS = -std=c++17 -w -O0 -ggdb
  LDLIBS += $(LDLIBSXTRA)
#   CXXFLAGS = -std=c++17 -w -O0 -g -DDEBUG
  sdl2:LDLIBS += $(SDL2FLAGS)
else
  CXXFLAGS = -std=c++14 -stdlib=libc++ -w -O0 -ggdb3
#   CXXFLAGS = -std=c++14 -w -O0 -ggdb3
#   CXXFLAGS = -std=c++14 -stdlib=libc++ -w -O0 -ggdb -DDEBUG
  sdl2:LDLIBS += $(SDL2FLAGS)
endif

ifeq ($(THREAD),true)
  CXXFLAGS += $(THREADFLAGS)
  LDLIBS += $(LDTHEADFLAGS)
endif

ifeq ($(GMP),true)
  LDLIBS += $(GMPFLAGS)
endif

ifeq ($(PED),true)
  CXXFLAGS += $(PEDFLAGS)
endif

#Won't be able to find header files without these.
#Don't have to worry about directory structures..
CPPFLAGS = -I"/home/elitebook/Desktop/Big Mandlebrot/Final Project/FileHandling/"\
-I"/home/elitebook/Desktop/Big Mandlebrot/Final Project/Graphics/"\
-I"/home/elitebook/Desktop/Big Mandlebrot/Final Project"\
-I"/home/elitebook/Desktop/Big Mandlebrot/Final Project/Ancillaries/"


CXXFLAGS += $(DEBUGFLAG)

# vvvvv important vvvvvv
SRCS4 := $(wildcard $(DIR3)*.cpp)
SRCS3 := $(wildcard $(DIR2)*.cpp)
SRCS2 := $(wildcard $(DIR1)*.cpp)
SRCS1 := $(wildcard *.cpp)
# So that sources are *.cpp /FileHandling/*.cpp & /Graphics/*.cpp
SRCS = $(SRCS1) $(SRCS2) $(SRCS3) $(SRCS4)

# However all the object files are in the same dir as the linker so have
# to remove FileHandling/ and Graphics/ from the source files to get correct
# object file names , ie one without directory precursors

OBJS := $(patsubst %.cpp,%.o,$(SRCS))
OBJS := $(patsubst $(DIR1)%,%,$(OBJS))
OBJS := $(patsubst $(DIR2)%,%,$(OBJS))
OBJS := $(patsubst $(DIR3)%,%,$(OBJS))
# ^^^^^ important ^^^^^^

# check srcs and objs are ok...
$(info SRCS are $(SRCS))
$(info OBJS are $(OBJS))

sdl2: $(TARGET)
default:$(TARGET)

%.o: %.cpp
	$(CC) $(CPPFLAGS) $(CXXFLAGS) -c $< ; pwd

$(TARGET): $(OBJS)
	g++-7 $^ $(LDLIBS) -o $@
