#ifndef IM_CPP
#define IM_CPP

#include "FileHandler.hpp"
#include "Constants.hpp"
#include "RGB.hpp"
#include "ImageParameters.hpp"
#include "ThreadPool.hpp"
#include "PartialCalculator.hpp"

#include <SDL2/SDL.h>
#include <gmp.h>

#include <array>
#include <string>


// template class std::array<std::array<int, Graphics::TILE_SIZE_PXLS>, Graphics::TILE_SIZE_PXLS>;

struct Current_Image
{
  std::string name;
  ImageParams IPs;
};

class ImageManager : private FileHandler
{
  public:
  using string_vec = std::vector<std::string>;

  ImageManager(bool automatic);

  static std::array<std::array<int, Graphics::TILE_SIZE_PXLS>, Graphics::TILE_SIZE_PXLS> Itrtn_Vals;
  static bool calculating_ItrnVals, image_saved;

  int maxIterations = Default::MIN_ITERATIONS;
  bool finished;

  bool ProcessImagesOnList(SDL_Renderer* renderer, std::vector<std::string>& list);
  bool RenderAndSaveImageNData(SDL_Renderer* renderer, Current_Image & imageToProcess, bool printed);

// ----------------------------------------------------------------------
  private:

  static constexpr int8_t NOT_CALCULATED = -1;

  ThreadPool pool;
  static std::array<std::future<void>, Default::MAX_THREADS> futures;
  static std::array<partial_calculator, Default::MAX_THREADS> images;

  static Current_Image currentImage;                            // The image currently being calculated

  bool Automatic;
  void renderMandelbrot(SDL_Renderer* renderer, const RGB & colour, uint8_t xstep, uint8_t ystep);
  int isImageAll_1_Colour(uint8_t xstep, uint8_t ystep);
  void set_Itrn_Vals_NotCalcd();

#ifndef DDEBUG
// Used for gdb as I can never get the [] operator to work eg (in GDB) call pIV(0, 5)

  uint32_t pIV(size_t x, size_t y);
#endif

};

#endif