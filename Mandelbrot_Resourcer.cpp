#include "Constants.hpp"
#include "ImageParameters.hpp"
#include "Mandelbrot_Resourcer.hpp"
#include "Position.hpp"
#include "LetterCoords.hpp"
#include "SmallTexture.hpp"

#include <utility>
#include <string>
#include <algorithm>

#include <stdio.h>
#if __cplusplus == 201703L
  #include <experimental/filesystem>
#endif

#include <SDL2/SDL.h>

// Debug flag needs assigning
template class std::array<SmallTexture, Mandelbrot_Resourcer::MAX_TILES>;

Mandelbrot_Resourcer::stringVec Mandelbrot_Resourcer::FilesToBeCalculated_Automatically;

Mandelbrot_Resourcer::stringVec Mandelbrot_Resourcer::images2B_calculated;


void MR_utils::prepend_zeros(std::string& inStr, const uint8_t numberOfZeros)
{
  while (inStr.length() < numberOfZeros)                        //Prefixing zeros to level number
  {
    inStr = "0" + inStr;
  }
  return;
}

template<typename T>
std::string MR_utils::number_to_string_with_leading_zeros(const T number, const uint8_t numberOfZeros)
{
  std::string returnStr{std::to_string(number)};
  MR_utils::prepend_zeros(returnStr, numberOfZeros);
  return returnStr;
}



Mandelbrot_Resourcer::Mandelbrot_Resourcer(SDL_Renderer* renderer, bool automatic)
                    :image_manager(automatic)
                    ,Automatic{automatic}
{
  blank_texture.createBlankTexture(renderer);
}

void Mandelbrot_Resourcer::Texturise(SDL_Renderer* renderer, Position& position,
                                      const UserDefined_s& user_params)
{
  createSmallTextures(renderer, position, user_params);
  tileTextures(renderer);
  return;
}

void Mandelbrot_Resourcer::Process_Uncalculated_Images(SDL_Renderer* renderer)
{
  image_manager.ProcessImagesOnList(renderer, images2B_calculated);
  return;
}

void Mandelbrot_Resourcer::DoNewImagesNeed_Rendering(SDL_Renderer* renderer,
                                                      Position& position,
                                                      const UserDefined_s& user_params)
{
  if (!image_manager.finished &&image_manager.image_saved)
  {
      OrganiseTileData(position);
      Texturise(renderer, position, user_params);
      image_manager.finished = true;
  }
  return;
}

void Mandelbrot_Resourcer::CreateListOfFilesToCalcd()
{
  DirContents(std::string(Default::PATH));
  constexpr bool DONT_OUTPUT = false;
  stringVec all_files_in_dir{CreateList(DONT_OUTPUT, FileHandler::File)};
  stringVec all_log_files{all_files_in_dir.size()};

// Copy the names of all the logfiles to all_log_files by searching through all_files and
// looking for the log file extension .mlg
  auto it = std::copy_if(all_files_in_dir.begin(),all_files_in_dir.end(),
                all_log_files.begin(),
                [&all_files_in_dir](std::string& str)
                {
                  return (str.find(".mlg") != std::string::npos);
                });

  all_log_files.resize(std::distance(all_log_files.begin(), it));

  for(auto &it  : all_log_files)
  {
    if (it != std::string(Default::MAIN_LOG_FILENAME))
    {
      ImageParams tempIPs;
      std::string tempFileName{(Default::PATH + it)};
      tempIPs.Load_IPs(tempFileName);
      if ((tempIPs.UserDefined.imageState == UserDefined_s::NOT_CALCULATED) ||
        (tempIPs.UserDefined.imageState == UserDefined_s::PARTIALLY_CALCULATED))
      {
        Mandelbrot_Resourcer::FilesToBeCalculated_Automatically.push_back(it);
        std::cout<<it<<" added to FilesToBeCalculated_Automatically\n";
      }
      else std::cerr<<"File: "<<it<<" completed\n";
    }
  }

// Finally turn them into identy strings for images2B_calculated by removing ".mlg"
  std::for_each( Mandelbrot_Resourcer::FilesToBeCalculated_Automatically.begin(),
                  Mandelbrot_Resourcer::FilesToBeCalculated_Automatically.end(),
                  [this](std::string& ident_string)
                  {
                    ident_string = ChangeFileExtsn(ident_string, "");
                    return;
                  } );
  return;
}

void Mandelbrot_Resourcer::extract_filesByLevel(uint32_t level)
{
  Mandelbrot_Resourcer::images2B_calculated.clear();
  using namespace MR_utils;
  const std::string level_Str{number_to_string_with_leading_zeros(level,
                                                                  Default::MAX_LEVEL_DIGITS)};
  std::copy_if(Mandelbrot_Resourcer::FilesToBeCalculated_Automatically.begin(),
                Mandelbrot_Resourcer::FilesToBeCalculated_Automatically.end(),
                std::back_inserter(Mandelbrot_Resourcer::images2B_calculated),
                [&level_Str] (std::string& str)
                {
                  return (str.find(level_Str) != std::string::npos);
                });

  auto auto_calcd_files = &Mandelbrot_Resourcer::FilesToBeCalculated_Automatically;

  auto_calcd_files->erase(std::remove_if( auto_calcd_files->begin(),
                                  auto_calcd_files->end(),
                                  [&level_Str](std::string& inStr)
                                  {
                                    return (inStr.find(level_Str) != std::string::npos);
                                  }),
                                  auto_calcd_files->end() );
  return;
}

void Mandelbrot_Resourcer::OrganiseTileData(const Position& position)
{
  sortEdges(position);
  getFilesAndTileSpecs(position);
  WidthPxls = columns * Graphics::TILE_SIZE_PXLS;
  HeightPxls = rows * Graphics::TILE_SIZE_PXLS;
  ImageParams::ImageParamChanged = true;
  return;
}

void Mandelbrot_Resourcer::UpDateImagesToBeCalculated(Position& position)
{
  if (!FilesToBeCalculated_Automatically.empty())
  {
    while (images2B_calculated.empty())
    {
      extract_filesByLevel(position.fileLevel);
      ++position.fileLevel;
    }
// So that fileLevel is in sync with files that are being calculated
    // --position.fileLevel Don't think I need it;
  }
  else if (images2B_calculated.empty() && !ImageManager::calculating_ItrnVals) throw std::runtime_error ("There aren't any files to be calculted, terminating.\n");
  return;
}


// ----------------------------------- PRIVATE METHODS -----------------------------------

void Mandelbrot_Resourcer::sortEdges(const Position& position)
{
  if (position.fileLevel == 1) yMAX.PowerTwo(0);
  else yMAX.PowerTwo(position.fileLevel - 2);
  xMAX.PowerTwo(position.fileLevel - 1);
  lhsLC = position.fileSystem_x;
  if (position.fileSystem_x != Position::FILE_SYS_MIN) --lhsLC;
  rhsLC = position.fileSystem_x;
  if (position.fileSystem_x != xMAX) ++rhsLC;
  topLC = position.fileSystem_y;
  bottomLC = position.fileSystem_y;
  if (position.small_TextureBelowXaxis)
  {
    if (position.fileSystem_y != yMAX) ++topLC;
    if (position.fileSystem_y != Position::FILE_SYS_MIN) --bottomLC;
  }
  else
  {
    if (position.fileSystem_y != Position::FILE_SYS_MIN) --topLC;
    if (position.fileSystem_y != yMAX) ++bottomLC;
  }
// If near the axis this'll give two lines of images with the same filename which means
// one needs to be flipped
  return;
}

void Mandelbrot_Resourcer::getFilesAndTileSpecs(const Position& position)
{
  // std::for_each ??
  for (size_t it = 0; it < MAX_TILES; ++it) small_textures[it].ResetFileNFlips();


  ltcd::LetterCoords itTB = topLC, botLimLC = bottomLC;
  tileCount = 0;
  if (position.small_TextureBelowXaxis) --botLimLC;
  else ++botLimLC;
  rows = 0;                                                     // all this botLimLC = bottomLC, ++botLimLC
  bool lineFlipped = false, TBitteratorAdjusted = false;
  while (itTB != botLimLC)                                      // is because the ++ operator works even when
  {                                                             // if position.fileSystem_x = ++a ++ will be done even thogh it's
    ltcd::LetterCoords itLR = lhsLC, rightLimLC = rhsLC;     // !"£$%^&
    ++rightLimLC;
    columns = 0;
    while(itLR != rightLimLC)                                   // for loop not available for LetterCoords !
    {
      if(position.small_TextureBelowXaxis)
      {
        if (position.fileSystem_y != yMAX)
          small_textures[tileCount].flipped = true;
        else
        {
          if (lineFlipped) small_textures[tileCount].flipped = true;
          if ( ((itTB == yMAX) && (itLR == rhsLC) ) && (position.fileLevel > 1))
            lineFlipped = true;
        }
      }
      else
      {
        if (position.fileSystem_y != yMAX) small_textures[tileCount].flipped = false;
        else
        {
          if (lineFlipped) small_textures[tileCount].flipped = true;
          if ( (itTB == yMAX) && (itLR == rhsLC) && (position.fileLevel > 1))
            lineFlipped = true;
        }
      }
      std::string numberStr{MR_utils::number_to_string_with_leading_zeros(position.fileLevel,
                                                                Default::MAX_LEVEL_DIGITS)};
      small_textures[tileCount].filename =
        Default::PATH + numberStr + (itTB.toString()) + (itLR.toString()) + ".mbt";
      assert(tileCount < MAX_TILES);                            // Just to check all hell hasn't broken out
      ++tileCount;
      ++itLR;
      ++columns;                                                // should reach a max value of three
    } // ENDOF while(itLR != rightLimLC)
    if (lineFlipped)
    {
      if (position.small_TextureBelowXaxis)
      {
        if (TBitteratorAdjusted) --itTB;
        else TBitteratorAdjusted = true;
      }
      else
      {
        if (TBitteratorAdjusted) ++itTB;
        else TBitteratorAdjusted = true;
      }
    }
    else
    {
      if(position.small_TextureBelowXaxis) --itTB;
      else ++itTB;
    }
    ++rows;                                                      // should reach a max value of three
  } // ENDOF while (itTB != botLimLC)
  // Calc_MPFs(position);
  return;
}

void Mandelbrot_Resourcer::createSmallTextures(SDL_Renderer* renderer, Position& position,
                                                const UserDefined_s& user_params)
{
  if (!Automatic)
  {
    assert(tileCount <= MAX_TILES);
    assert(tileCount = (rows * columns));

    bool calculated_n_saved = true;
    for (size_t it = 0; it < tileCount; ++it)
    {

  // If C++17 filesystem::exists(small_textures[it].filename)
      if ( ! DoesFileExist(small_textures[it].filename))
      {
        calculated_n_saved = false;                               // current small_texture isn't
  // "Image is being calculated" texture assignment and saves logfile
        assignBlank_toSmallTexture(renderer, position, user_params, it);
      }

  // Just better to load log file: IPs_alreadySet was casuing more hassel than it saved
      std::string temp_filename(ChangeFileExtsn(small_textures[it].filename, ".mlg"));
      small_textures[it].imagePs.Load_IPs(temp_filename);

      if (small_textures[it].imagePs.UserDefined != user_params)
        std::cerr<<"small_textures.IPs. DON'T equal current IPs: SEND SIGNAL"<<
                    " DO YOU WANT TO RECALCULATE?\n";
      else std::cerr<<"Comparison made of IPs IMAGE AND USER PARAMS THE SAME\n";

  // **************************************************************
  // *********************** BIG TO DO ****************************
  // **************************************************************

  // WHAT IF THERE'S NO LOG FILE,  BLANK TEXTURE HAS BEEN CREATE
  // BUT THERE'S NO LOGFILE AT THIS POINT

  //  --------------- There should be a logfile ! ---------------

  // **************************************************************
  // *********************  END OF TO DO  *************************
  // **************************************************************


  // Automatic Comments for automatic process
  // this will take care of some of my concerns re copies,  one image is 'C' it
  // won't be redone

      if ( (small_textures[it].imagePs.UserDefined.imageState == UserDefined_s::NOT_CALCULATED)
             /*&& !calculated_n_saved */)
      {
        std::string ident_string{ RemovePath(small_textures[it].filename, Default::PATH) };
        ident_string = ChangeFileExtsn(ident_string, "");

  // This should be find()
        bool existsAlready = false;
        for(const auto &image_off_the_list  : images2B_calculated)
        {
          if (ident_string == image_off_the_list) existsAlready = true;
        }
        if (!existsAlready)
        {
          images2B_calculated.push_back(ident_string);
        }
  // end of find()
      }
      if ( !small_textures[it].CreateTextureFromPNG(renderer, small_textures[it].filename))
      {
        uint64_t oneColourOrItrtn;
        if (!small_textures[it].Load_OneColourInfo(oneColourOrItrtn))
        {
            throw std::runtime_error
              ("Complete Failure to create small_textures at"
                " Mandelbrot_Resourcer::createSmallTextures()");
        }
        else
        {
          RGB colour;

  // Don't think I need this because when all one colour is saved the appropriate log file is saved
  // thus when small_textures.Load_IPs() that should give the right colour value..

  // ************ CURRENT IP small_texture IP CLASH ************
          colour.assignValues(small_textures[it].imagePs.UserDefined.maxIterations);
          uint8_t R = colour.Red[oneColourOrItrtn];
          uint8_t G = colour.Green[oneColourOrItrtn];
          uint8_t B = colour.Blue[oneColourOrItrtn];
          if (!small_textures[it].Assign1_Col(renderer, R, G, B, Graphics::TILE_SIZE_PXLS, Graphics::TILE_SIZE_PXLS))
          {
            throw std::runtime_error
              ("Complete failure assign a colour (prob black) at Mandelbrot_Resourcer::Update");
          }
        }
      }   // ENDOF if( !small_textures[it].CreateTextureFromPNG(renderer, small_textures[it].filename))
    }   // ENDOF for loop near beginning of function
  }    // ENDOF if (!Automatic)
  else  // ie if (Automatic)
  {
    UpDateImagesToBeCalculated(position);
  }
  return;
}

void Mandelbrot_Resourcer::assignBlank_toSmallTexture(SDL_Renderer* renderer, const Position& position,
                                        UserDefined_s user_params,  size_t index)
{

// Automatic Comments for automatic process
// WHICH BITS OF THIS FUNCTION DO I NEED TO DO !

// actually need to check logs first as double check and then if created need
// to check on state: Partially calculated or not
// Work out what LHSide, R, Top and Bottom should be.
// Check to see if there's a log file, if so load values and double check.
// else create one. Once you've got L, R, T, B you can create the image.

  user_params.imageState = UserDefined_s::NOT_CALCULATED;                         // Because image is uncompleted, not drawn
  small_textures[index].imagePs.Initialise(0.0, 0.0, 0.0, 0.0, 0.0, user_params);
  small_textures[index].Calc_MPFs(position.fileLevel);

  std::string ident_string{ RemovePath(small_textures[index].filename, Default::PATH) };
  ident_string = ChangeFileExtsn(ident_string, "");
  images2B_calculated.push_back(ident_string);

  std::string logFilename = ChangeFileExtsn(small_textures[index].filename, ".mlg");
  SaveType_inFile(logFilename, small_textures[index].imagePs.MPFs, FileHandler::NEW_FILE);
  SaveType_inFile(logFilename, user_params);

  SDL_RenderClear(renderer);
  small_textures[index].PrepareToBeRendered_To(renderer, Graphics::TILE_SIZE_PXLS, Graphics::TILE_SIZE_PXLS);
  blank_texture.GWTexture.RenderSrcRct(renderer, 0, 0, (Graphics::TILE_SIZE_PXLS - 1), (Graphics::TILE_SIZE_PXLS - 1), true);
// Haven't checked for failures (at multiple points)
  small_textures[index].SaveAsPNG(renderer, small_textures[index].filename,
                                    Graphics::TILE_SIZE_PXLS, Graphics::TILE_SIZE_PXLS); //destroys small_texture
  return;
}



void Mandelbrot_Resourcer::tileTextures(SDL_Renderer* renderer)
{
  if (!Automatic)
  {
    size_t it = 0;
    CompleteTexture.PrepareToBeRendered_To(renderer, (Graphics::TILE_SIZE_PXLS * 3), (Graphics::TILE_SIZE_PXLS * 3));

    for (size_t itRow = 0; itRow < rows; ++itRow)
    {
      for (size_t itCol = 0; itCol < columns; ++itCol)
      {
        const uint16_t temp_x = (Graphics::TILE_SIZE_PXLS*itCol), temp_y = (Graphics::TILE_SIZE_PXLS*itRow);

  // Can't use RenderSrcRect here or it don't work
        small_textures[it].Render(renderer, temp_x, temp_y, Graphics::TILE_SIZE_PXLS, Graphics::TILE_SIZE_PXLS,
                                        small_textures[it].flipped);
        ++it;
      }
    }
    assert(it == tileCount);
  }
  return;
}

void Mandelbrot_Resourcer::pST(size_t index)
{
  std::cout<<"\nsmall_textures[index].filename: "<<small_textures[index].filename<<"\n";
  std::cout<<"small_textures[index].flipped: "<<small_textures[index].flipped<<"\n";
  std::cout<<small_textures[index].imagePs<<"\n";
  return;
}
