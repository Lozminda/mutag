#ifndef USERINTERFACE_HPP
#define USERINTERFACE_HPP

#include "Constants.hpp"

#include "DeleterTemplates.hpp"
#include "RGB.hpp"
#include "GTexture.hpp"
#include "LetterCoords.hpp"
#include "Position.hpp"
#include "Mandelbrot_Resourcer.hpp"
#include "SDLEnvironment.hpp"
#include "InfoToggleEnum.hpp"
#include "ImageParameters.hpp"
#include "FileHandler.hpp"

#include <string>
#include <array>
#include <vector>
// UserInterface takes .png files, tiles them together and then allows the user to move horizonaltally
// around that texture or zoom in or out. Calculating position from series of files and converting them
// into a 3D simulation.
// It also deals with all the boundrys, if the user moves out of boundry a selection of pictures
// is tiled together. Similarly with zoom in and out.
// .png filenames are in the format 0000002aaaaabaaaaaae.mbt. The number gives the current level,
// the first six letters the users x position and second group of letters the y.
// In the above example that image is on level 2, 2nd column, fifth row.

// aaaaaaaaaaaa aaaaaabaaaaaaa aaaaacaaaaaaa aaaaaadaaaaaaa aaaaaaaeaaaaaaa aaaaaaafaaaaaaa
// aaaaaaaaaaab aaaaaabaaaaaab aaaaacaaaaaab aaaaaadaaaaaab aaaaaaaeaaaaaab aaaaaaafaaaaaab
// aaaaaaaaaaac aaaaaabaaaaaac aaaaacaaaaaac aaaaaadaaaaaac aaaaaaaeaaaaaac aaaaaaafaaaaaac
// aaaaaaaaaaad aaaaaabaaaaaad aaaaacaaaaaad aaaaaadaaaaaad aaaaaaaeaaaaaad aaaaaaafaaaaaad
// aaaaaaaaaaae aaaaaabaaaaaae aaaaacaaaaaae aaaaaadaaaaaae aaaaaaaeaaaaaae aaaaaaafaaaaaae
// aaaaaaaaaaaf aaaaaabaaaaaaf aaaaacaaaaaaf aaaaaadaaaaaaf aaaaaaaeaaaaaaf aaaaaaafaaaaaaf

// (The right hand column, in the letter coord system, is least sig. fig.)

// The Mandelbrot set being symmetrical about the x axis mean that space can be saved on disc, and
// if the user is below the x axis the above x axis images can be flipped. The letter coordinates follow
// a cartesian coordinate system, where as the screen's 0,0 point is top left. This along with image
// flipping introduces some complexity.
// This setup is contingent on the file system being correctly organised, if it becomes corrupted in some
// way then the whole thing will fail.

// template class std::vector;
// template class std::string;

class UserInterface: protected FileHandler
{

  public:

  UserInterface(bool automatic = false);

  UserInterface(const UserInterface&) = delete;
  UserInterface & operator=(const UserInterface&) = delete;
  ~UserInterface() = default;

  void MainLoop();

  private:

// These need sorting out,  prob in the wrong place
  static constexpr int X_ADJUSTMENT = 283;                             // mandelbrotVP.x + 1
  static constexpr int Y_ADJUSTMENT = 3;                               // mandelbrotVP.y - 1
  static constexpr uint16_t SCREEN_CENTRE = static_cast<uint16_t>(Graphics::TILE_SIZE_PXLS / 2); // !! CONSANTS.HPP !!

// Renderer, Window environment, Displays mandelbrot_resourcer, "how to"
// info
  SDLEnvironment graphicsWindow;

  Information_options info_options;
// This loads textures and tiles them together based on where you are in the filesystem
// Loads or creates Mandelbrot textures to be displayed by graphicsWindow
  Mandelbrot_Resourcer mandelbrot_resourcer;

  bool REALBelowXaxis;
  bool Automatic;

  int posOnMandel_textureX, posOnMandel_textureY;

  void assign_values(Position& pos, ImageParams& imagePs);

  void mouseHandler(SDL_Event& sdl_event,  Position& position, UserDefined_s& user_params);
  int mouseStateX, mouseStateY;
  static bool left_MseButtonPressed;
  void mouseButtonHandler(bool button_pressed, Position & position, UserDefined_s& user_params);


  bool keyBoardHandler(SDL_Event& sdl_event,  Position& position, UserDefined_s& user_params);

  void left(Position& position, UserDefined_s& user_params);

// As above for right(), up() & down()
  void right(Position& position, UserDefined_s& user_params); //const ?
  void up(Position& position, UserDefined_s& user_params);//const ?
  void down(Position& position, UserDefined_s& user_params);//const ?

  void zoomIn(Position& position, UserDefined_s& user_params);//const ?
  void zoomOut(Position& position, UserDefined_s& user_params);//const ?


  void getPositionOnMandel_texture(Position& position);                        // ONCE TESTING HAS FINISHED CAN
  void calcViewTopLeft_XY_fromPositionOnMandel_texture(Position& position);                           // GET RID OF BOOL PARAM

  void toggle_info();//const ?
  void toggle_imagePercentage(UserDefined_s& user_params);//const ?

  // void printOut() const;
};

#endif