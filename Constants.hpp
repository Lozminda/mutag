#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "LetterCoords.hpp"

#include <cstdint>

// define your own namespace to hold constants
// letter coords MIN to go in here ?

// Image states: C omplete, P artial, N othing

// ************************** TO DO *****************************
// Having put almost all the constants in here some do belong to
// just one class, so those need to be 'removed' back to whence
// they came

// *********************  END OF TO DO  *************************

namespace Graphics
{
constexpr uint16_t TILE_SIZE_PXLS = 680;

}

namespace Default
{

// Just stringTextures
constexpr uint8_t FRAME_THICKNESS = 12;

constexpr uint8_t frameThickness = 3;

// Only in ImageManager !
constexpr int MANY_COLOURS = -1;
constexpr size_t MAX_THREADS = 4;                                  //Maybe should be just in ImageManager.hpp

// ImageParams and smallTextures
constexpr double LEFT = -2.0;
constexpr double RIGHT = 0.5;
constexpr double TOP = 1.25;
constexpr double BOTTOM = -1.25;

constexpr uint32_t MIN_ITERATIONS = 250;
// U_I only
constexpr uint32_t MAX_ITERATIONS = 2147483647;
constexpr uint32_t BASELINE_ITERATIONS = 1000;

// A few
constexpr uint8_t MAX_LEVEL_DIGITS = 6;                         // For large (final) texture

// Position Defaults
constexpr int32_t POS_ON_LARGE_TEXTURE_X = 0;
constexpr int32_t POS_ON_LARGE_TEXTURE_Y = 0;
constexpr int32_t MOUSE_POS_X = 340;
constexpr int32_t MOUSE_POS_Y = 340;
constexpr int32_t START_LEVEL = 2;
constexpr bool FLIPPED = false;
constexpr int32_t CROSSHAIR_SPEED = 68;
// The File Position defaults are a bit unecessary as LetterCoords default to 'aaaaaa' anyway
// however handy to have all in one place for testing purposes
static std::string FILE_POS_X{"aaaaaa"};
static std::string FILE_POS_Y{"aaaaaa"};

// ImageParameter.Defaults
constexpr uint8_t RESOLUTION_X = 1;
constexpr uint8_t RESOLUTION_Y = 3;
constexpr char IMAGE_STATE = 'N';
constexpr uint32_t START_ITERATIONS = 250;

// **************************************************************************
//                   Different testing directories
// **************************************************************************
// constexpr char PATH[] = "/home/toshiba/Desktop/Testing Groond (temp out of Generator Stage 2)/";
// constexpr char PATH[] = "/home/elitebook/Desktop/Testing Groond (temp out of Generator Stage 2)/";
// constexpr char PATH[] = "/home/toshiba/Desktop/Test MB FP/";
// constexpr char PATH[] = "/home/elitebook/Desktop/Test MB FP/";
constexpr char PATH[] = "/home/elitebook/Desktop/Test MB for all 1 col/";
constexpr char MAIN_LOG_FILENAME[] = "mainLogFile.mlg";

}
#endif
// /home/toshiba/Desktop/Big Mandlebrot/Final Project/Constants.hpp