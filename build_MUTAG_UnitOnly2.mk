# -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self  -Wmissing-declarations -Wmissing-include-dirs -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-overflow=5 -Wswitch-default -Wundef -Werror -Wno-unused
# This variable might be 'overwritten' by a command line arg
CC = clang++-3.8
GMP = false
THREAD = false
PED = false
SDL2FLAGS = -lSDL2 -lSDL2_image -lSDL2_ttf
GMPFLAGS =  -lgmp -lgmpxx
THREADFLAGS = -pthread
LDTHEADFLAGS = -lpthread
PEDFLAGS = -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self  -Wmissing-declarations -Wmissing-include-dirs -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-overflow=5 -Wswitch-default -Wundef -Werror -Wno-unused
DEBUGFLAG =
SANITIZEFLAGS =  -fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls
LDLIBSXTRA = -lstdc++fs

# LDFLAGs for -L rather than -l LDFLAGS = ...
LDLIBS = -lc++ -lm -lgcc_s -lgcc -lc

# Sorting the different flags out for different compilers and modes
ifeq ($(CC),g++-7)
  CXXFLAGS = -std=c++17 -w -O0 -ggdb
  LDLIBS += $(LDLIBSXTRA)
#   CXXFLAGS = -std=c++17 -w -O0 -g -DDEBUG
  sdl2:LDLIBS += $(SDL2FLAGS)
else
  CXXFLAGS = -std=c++14 -stdlib=libc++ -w -O0 -ggdb3
# 	CXXFLAGS = -std=c++14 -stdlib=libc++ -w -O0 -ggdb -DDEBUG
  sdl2:LDLIBS += $(SDL2FLAGS)
endif

ifeq ($(THREAD),true)
  CXXFLAGS += $(THREADFLAGS)
  LDLIBS += $(LDTHEADFLAGS)
endif

ifeq ($(GMP),true)
  LDLIBS += $(GMPFLAGS)
endif

ifeq ($(PED),true)
  CXXFLAGS += $(PEDFLAGS)
endif

CXXFLAGS += $(DEBUGFLAG)

SRCS := $(addsuffix .cpp,$(TARGET))
OBJS := $(addsuffix .o,$(TARGET))

sdl2: $(TARGET)
default:$(TARGET)

%.o: %.cpp
	$(CC) $(CPPFLAGS) $(CXXFLAGS) -c $< ; pwd

# $(TARGET): $(OBJS)
# 	g++-7 $^ $(LDLIBS) -o $@
