// https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Rh-mi-interface
// C 135 Note: Such interfaces are typically abstract classes

#ifndef MANDELBROT_RESOURCER_HPP
#define MANDELBROT_RESOURCER_HPP

#include "Constants.hpp"
#include "LetterCoords.hpp"
#include "GTexture.hpp"
#include "BlankTexture.hpp"
#include "RGB.hpp"
#include "FileHandler.hpp"
#include "Position.hpp"
#include "IM.hpp"
#include "ImageParameters.hpp"                               // For MPF_s explicitly
#include "SmallTexture.hpp"

#include <string>
#include <cstdint>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <gmp.h>

// Filenames of images are stored as Row letter follow by Columns letter this gives
// y value followed by x value. These letter co-ords are stored in a char array with
// the smallest stored as zero. In order to get the filename they've goto to be printed
// out backwards eg [0]='c', [1]='a', [2]='a', [3]='a', [4]='a', [5]='a', [6]='a',
// 'caaaaa' which in a large level is close to LHS in the file name would become
// aaaaac and if it's the x co-ord it would be in the second half of the filename:
// 0000007yyyyyyaaaaac

// aaaaaa, aaaaab , aaaaac , aaaaad , aaaaae , aaaaaf etc


namespace MR_utils
{

void prepend_zeros(std::string& inStr, const uint8_t numberOfZeros);

template<typename T>
std::string number_to_string_with_leading_zeros(const T number, const uint8_t numberOfZeros);

}

class Mandelbrot_Resourcer: protected FileHandler
{

public:
// Don't need these coz have stringVec in FileHandler and both should be private
  using string_vec = std::vector<std::string>;
  static string_vec images2B_calculated;
  static stringVec FilesToBeCalculated_Automatically;
  
  Mandelbrot_Resourcer(SDL_Renderer* renderer, bool automatic = false);

  Mandelbrot_Resourcer(const Mandelbrot_Resourcer&) = delete;
  Mandelbrot_Resourcer & operator=(const Mandelbrot_Resourcer&) = delete;
  ~Mandelbrot_Resourcer() = default;



// For the lower levels or in the corners CompleteTexture might be quite small (680x680)
// on level one the same size as smallTxtr or 2040*2040. Is public so it can be
// displayed by UserInterface, THOUGH MAYBE THIS CLASS CAN HANDLE IT

  hsdl::GTexture CompleteTexture;


  ImageManager image_manager;

// Current dimensons of Texture
  uint16_t WidthPxls;
  uint16_t HeightPxls;

  ltcd::LetterCoords xMAX;
  ltcd::LetterCoords yMAX;

// Does the graphical part of rendering smaller textures to Texture
  void Texturise(SDL_Renderer* renderer, Position& position,
                  const UserDefined_s& user_params);

// Works out the borders based on fileSystem_x&y and then come up with the names o all the .png
// files that would fill those borders, typically 1, 4, 6 or 9 files
  void OrganiseTileData(const Position& position);

  void Process_Uncalculated_Images(SDL_Renderer* renderer);

  void CreateListOfFilesToCalcd();

  void DoNewImagesNeed_Rendering(SDL_Renderer* renderer,Position& position,
                                const UserDefined_s& user_params);

  void UpDateImagesToBeCalculated(Position& position);

  void pST(size_t index);

private:
  static const uint8_t MAX_TILES = 9;                         // For large (final) texture

// Left Hand Side, Right Hand Side, Top, Bottom of the Texture to be generated in LetterCoords
  ltcd::LetterCoords lhsLC;
  ltcd::LetterCoords rhsLC;
  ltcd::LetterCoords topLC;
  ltcd::LetterCoords bottomLC;

// smallTextures are tiled together to form large Texture in a varitety of configurations
  std::array <SmallTexture, MAX_TILES> small_textures;

// large Texture will be made up of a maximum of 9 tiles/.png files (tileCount reaching a
// max of MAX_TILES) therefore rows and columns max is 3.

  uint8_t tileCount, rows, columns;

// This is the texture with 'Current Image is being Calculated' on it
  BlankTexture blank_texture;

  bool Automatic;

// The blank texture has 'Current Image is being Calculated' written on it and
// assignBlank_toSmallTexture() assigns that to a small_textures[index]

  void assignBlank_toSmallTexture(SDL_Renderer* renderer, const Position& position,
                                  UserDefined_s user_params,  size_t index);


// sortEdges() calculates the boundries; lhsLC, rhsLC, topLC and bottomLC so that sort
// filenames can implement the above structure.

  void sortEdges(const Position& position);


// The logic for all this is a bit of a mine field. Firstly it's got to work if there's only
// one image on the level or four or nine or above, nine being the max. At the fileSystem_x axis which is
// yMAX there's flipping of the image, so with each filename there's whether the image should
// be flipped or not..All the flags are to account for that. Oh did I mention corners and near
// boundrys ?
// See before sortEdges() for more detail

  void getFilesAndTileSpecs(const Position& position);

  void extract_filesByLevel(uint32_t level);

// ----------------------------------- GRAPHICAL METHODS -----------------------------------
// Loads the .png files stored in filesN_flips and stores them as textures in smallTxtr[]
// or if the file is a textfile assigns the colour to the texture as give by the nuber of iterations
// TOTAL NUMBER OF ITERATIONS WILL ALSO HAVE TO BE STORED AS WE DELVE DEEPER INTO THE SET

  void createSmallTextures(SDL_Renderer* renderer, Position& position,
                            const UserDefined_s& user_params);


// smallTxtr[] is then tiled onto Texture for later renderering as specified by sortEdges()
// and getFilesAndTileSpecs()

  void tileTextures(SDL_Renderer* renderer);

};

#endif